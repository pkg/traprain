/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "turn-by-turn-notification.h"

/**
 * SECTION: traprain-guidance/turn-by-turn-notification.h
 * @title: TrpGuidanceTurnByTurnNotification
 * @short_description: turn-by-turn navigation notification to display
 *
 * #TrpGuidanceTurnByTurnNotification are received from the navigation service
 * for displaying.
 *
 * The guidance UI should connect on it the #GObject::notify or
 * TrpGuidanceTurnByTurnNotification::updated signals as the
 * properties of the notification can be updated by the sender while it's being
 * displayed.
 * It should also connect the #TrpGuidanceTurnByTurnNotification::close signal
 * to be notified if the navigation system decided to close the notification
 * earlier.
 *
 * Since: 0.1612.0
 */

/**
 * TrpGuidanceTurnByTurnNotification:
 *
 * #TrpGuidanceTurnByTurnNotification is an object representing a guidance
 * turn-by-turn notification to display.
 *
 * Since: 0.1612.0
 */

struct _TrpGuidanceTurnByTurnNotification
{
  GObject parent;

  gchar *summary;
  gchar *body;
  gchar *icon;
  gint expire_timeout; /* in milliseconds */
};

typedef enum {
  PROP_SUMMARY = 1,
  PROP_BODY,
  PROP_ICON,
  PROP_EXPIRE_TIMEOUT,
  /*< private >*/
  PROP_LAST = PROP_EXPIRE_TIMEOUT
} TrpGuidanceTurnByTurnNotificationProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_CLOSE,
  SIG_UPDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (TrpGuidanceTurnByTurnNotification, trp_guidance_turn_by_turn_notification, G_TYPE_OBJECT)

static void
trp_guidance_turn_by_turn_notification_get_property (GObject *object,
                                                     guint prop_id,
                                                     GValue *value,
                                                     GParamSpec *pspec)
{
  TrpGuidanceTurnByTurnNotification *self = TRP_GUIDANCE_TURN_BY_TURN_NOTIFICATION (object);

  switch ((TrpGuidanceTurnByTurnNotificationProperty) prop_id)
    {
    case PROP_SUMMARY:
      g_value_set_string (value, self->summary);
      break;
    case PROP_BODY:
      g_value_set_string (value, self->body);
      break;
    case PROP_ICON:
      g_value_set_string (value, self->icon);
      break;
    case PROP_EXPIRE_TIMEOUT:
      g_value_set_int (value, self->expire_timeout);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_guidance_turn_by_turn_notification_set_property (GObject *object,
                                                     guint prop_id,
                                                     const GValue *value,
                                                     GParamSpec *pspec)
{
  TrpGuidanceTurnByTurnNotification *self = TRP_GUIDANCE_TURN_BY_TURN_NOTIFICATION (object);

  switch ((TrpGuidanceTurnByTurnNotificationProperty) prop_id)
    {
      break;
    case PROP_SUMMARY:
      g_free (self->summary);
      self->summary = g_value_dup_string (value);
      break;
    case PROP_BODY:
      g_free (self->body);
      self->body = g_value_dup_string (value);
      break;
    case PROP_ICON:
      g_free (self->icon);
      self->icon = g_value_dup_string (value);
      break;
    case PROP_EXPIRE_TIMEOUT:
      self->expire_timeout = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_guidance_turn_by_turn_notification_dispose (GObject *object)
{
  TrpGuidanceTurnByTurnNotification *self = (TrpGuidanceTurnByTurnNotification *) object;

  g_clear_pointer (&self->summary, g_free);
  g_clear_pointer (&self->body, g_free);
  g_clear_pointer (&self->icon, g_free);

  G_OBJECT_CLASS (trp_guidance_turn_by_turn_notification_parent_class)
      ->dispose (object);
}

static void
trp_guidance_turn_by_turn_notification_class_init (TrpGuidanceTurnByTurnNotificationClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_guidance_turn_by_turn_notification_get_property;
  object_class->set_property = trp_guidance_turn_by_turn_notification_set_property;
  object_class->dispose = trp_guidance_turn_by_turn_notification_dispose;

  /**
   * TrpGuidanceTurnByTurnNotification:summary:
   *
   * The localised summary text of the notification describing briefly the
   * next action the driver has to take.
   * Something like "Turn right in 100 meters" for example.
   *
   * Since: 0.1612.0
   */
  properties[PROP_SUMMARY] = g_param_spec_string (
      "summary", "Summary", "Summary", NULL,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpGuidanceTurnByTurnNotification:body:
   *
   * The localised body text of the notification, or %NULL,
   * giving more details about the turn-by-turn
   * notification.
   * Something like "Take right on Goat Street".
   *
   * Since: 0.1612.0
   */
  properties[PROP_BODY] = g_param_spec_string (
      "body", "Body", "Body", NULL,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpGuidanceTurnByTurnNotification:icon:
   *
   * The icon name of the notification, or %NULL.
   *
   * Since: 0.1612.0
   */
  properties[PROP_ICON] = g_param_spec_string (
      "icon", "Icon", "Icon name", NULL,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpGuidanceTurnByTurnNotification:expire-timeout:
   *
   * The timeout time in milliseconds since the display of the notification at which the notification should automatically close.
   * If -1, the notification's expiration time is dependent on the notification server's settings, and may vary for the type of notification.
   * If 0, never expire.
   *
   * It's the responsibility of the notification server using this API to setup a
   * timeout according to this property and remove the notification accordingly.
   *
   * Since: 0.1612.0
   */
  properties[PROP_EXPIRE_TIMEOUT] = g_param_spec_int (
      "expire-timeout", "Expire Timeout", "Expire timeout, in milliseconds",
      -1, G_MAXINT, -1,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpGuidanceTurnByTurnNotification::close:
   * @self: a #TrpGuidanceTurnByTurnNotification
   *
   * Raised when the navigation system requested to stop displaying the notification.
   *
   * Since: 0.1612.0
   */
  signals[SIG_CLOSE] = g_signal_new ("close", TRP_GUIDANCE_TYPE_TURN_BY_TURN_NOTIFICATION,
                                     G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                     G_TYPE_NONE, 0);

  /**
   * TrpGuidanceTurnByTurnNotification::updated:
   * @self: a #TrpGuidanceTurnByTurnNotification
   *
   * Fired when the navigation system updated the notification. It can be
   * useful to rely on this signal rather than #GObject::notify to update
   * the notification completely in one shot.
   *
   * Since: 0.1703.1
   */
  signals[SIG_UPDATED] = g_signal_new ("updated", TRP_GUIDANCE_TYPE_TURN_BY_TURN_NOTIFICATION,
                                       G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                       G_TYPE_NONE, 0);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
trp_guidance_turn_by_turn_notification_init (TrpGuidanceTurnByTurnNotification *self)
{
}

/**
 * trp_guidance_turn_by_turn_notification_get_summary:
 * @self: a #TrpGuidanceTurnByTurnNotification
 *
 * Retrieve the #TrpGuidanceTurnByTurnNotification:summary: property of @self.
 *
 * Returns: (transfer none): the summary text of the notification
 * Since: 0.1612.0
 */
const gchar *
trp_guidance_turn_by_turn_notification_get_summary (TrpGuidanceTurnByTurnNotification *self)
{
  g_return_val_if_fail (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (self), NULL);

  return self->summary;
}

/**
 * trp_guidance_turn_by_turn_notification_get_body:
 * @self: a #TrpGuidanceTurnByTurnNotification
 *
 * Retrieve the #TrpGuidanceTurnByTurnNotification:body: property of @self.
 *
 * Returns: (transfer none) (nullable): the body text of the notification
 * Since: 0.1612.0
 */
const gchar *
trp_guidance_turn_by_turn_notification_get_body (TrpGuidanceTurnByTurnNotification *self)
{
  g_return_val_if_fail (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (self), NULL);

  return self->body;
}

/**
 * trp_guidance_turn_by_turn_notification_get_icon:
 * @self: a #TrpGuidanceTurnByTurnNotification
 *
 * Retrieve the #TrpGuidanceTurnByTurnNotification:icon: property of @self.
 *
 * Returns: (transfer none) (nullable): the icon text of the notification
 * Since: 0.1612.0
 */
const gchar *
trp_guidance_turn_by_turn_notification_get_icon (TrpGuidanceTurnByTurnNotification *self)
{
  g_return_val_if_fail (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (self), NULL);

  return self->icon;
}

/**
 * trp_guidance_turn_by_turn_notification_get_expire_timeout:
 * @self: a #TrpGuidanceTurnByTurnNotification
 *
 * Retrieve the #TrpGuidanceTurnByTurnNotification:expire-timeout: property of @self.
 *
 * Returns: the expire timeout, in milliseconds, of the notification
 * Since: 0.1612.0
 */
gint
trp_guidance_turn_by_turn_notification_get_expire_timeout (TrpGuidanceTurnByTurnNotification *self)
{
  g_return_val_if_fail (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (self), -1);

  return self->expire_timeout;
}
