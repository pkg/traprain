/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_GUIDANCE_TURN_BY_TURN_NOTIFICATION_H__
#define __TRAPRAIN_GUIDANCE_TURN_BY_TURN_NOTIFICATION_H__

#include <gio/gio.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define TRP_GUIDANCE_TYPE_TURN_BY_TURN_NOTIFICATION (trp_guidance_turn_by_turn_notification_get_type ())
G_DECLARE_FINAL_TYPE (TrpGuidanceTurnByTurnNotification, trp_guidance_turn_by_turn_notification, TRP_GUIDANCE, TURN_BY_TURN_NOTIFICATION, GObject)

const gchar *trp_guidance_turn_by_turn_notification_get_summary (TrpGuidanceTurnByTurnNotification *self);
const gchar *trp_guidance_turn_by_turn_notification_get_body (TrpGuidanceTurnByTurnNotification *self);
const gchar *trp_guidance_turn_by_turn_notification_get_icon (TrpGuidanceTurnByTurnNotification *self);
gint trp_guidance_turn_by_turn_notification_get_expire_timeout (TrpGuidanceTurnByTurnNotification *self);

G_END_DECLS

#endif /* __TRAPRAIN_GUIDANCE_TURN_BY_TURN_NOTIFICATION_H__ */
