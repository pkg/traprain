/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include <traprain-client/mock.h>

GMainLoop *loop = NULL;

gboolean update_start = FALSE;
GDateTime *start_date = NULL;
gboolean update_end = FALSE;
GDateTime *end_date = NULL;

guint pending = 0;
gboolean success = TRUE;

static void
start_cb (GObject *source,
          GAsyncResult *result,
          gpointer user_data)
{
  g_autoptr (GError) error = NULL;

  if (!trp_client_mock_set_start_time_finish (TRP_CLIENT_MOCK (source),
                                              result, &error))
    {
      g_warning ("Failed to set start time: %s", error->message);
      success = FALSE;
    }

  if (start_date != NULL)
    {
      g_autofree gchar *str = NULL;

      str = g_date_time_format (start_date, "%x %X");
      g_print ("Start time set to %s\n", str);
    }
  else
    {
      g_print ("Start time unset\n");
    }

  pending--;
  if (pending == 0)
    g_main_loop_quit (loop);
}

static void
end_cb (GObject *source,
        GAsyncResult *result,
        gpointer user_data)
{
  g_autoptr (GError) error = NULL;

  if (!trp_client_mock_set_estimated_end_time_finish (TRP_CLIENT_MOCK (source),
                                                      result, &error))
    {
      g_warning ("Failed to set estimated end time: %s", error->message);
      success = FALSE;
    }

  if (end_date != NULL)
    {
      g_autofree gchar *str = NULL;

      str = g_date_time_format (end_date, "%x %X");
      g_print ("Estimated end time set to %s\n", str);
    }
  else
    {
      g_print ("Estimated end time unset\n");
    }

  pending--;
  if (pending == 0)
    g_main_loop_quit (loop);
}

static void
mock_cb (GObject *source,
         GAsyncResult *result,
         gpointer user_data)
{
  g_autoptr (TrpClientMock) mock = NULL;
  g_autoptr (GError) error = NULL;

  mock = trp_client_mock_new_finish (result, &error);
  if (mock == NULL)
    {
      g_warning ("Failed to connect to mock service: %s", error->message);
      success = FALSE;
      g_main_loop_quit (loop);
      return;
    }

  if (update_start)
    {
      pending++;
      trp_client_mock_set_start_time_async (mock, start_date, start_cb, NULL);
    }

  if (update_end)
    {
      pending++;
      trp_client_mock_set_estimated_end_time_async (mock, end_date, end_cb, NULL);
    }
}

static GDateTime *
parse_time (const gchar *str)
{
  gint64 t;

  if (g_str_has_prefix (str, "now"))
    {
      g_autoptr (GDateTime) now = g_date_time_new_now_local ();

      if (g_strcmp0 (str, "now") == 0)
        return g_steal_pointer (&now);

      str += 3; /* skip 'now' */

      /* Add or substract seconds from the current time */
      if (str[0] == '+' ||
          str[0] == '-')
        {
          gint64 delta;

          delta = g_ascii_strtoll (str + 1, NULL, 10);
          if (delta == 0)
            return NULL;

          if (str[0] == '-')
            delta *= -1;

          return g_date_time_add_seconds (now, delta);
        }

      return NULL;
    }

  t = g_ascii_strtoll (str, NULL, 10);
  if (t != 0)
    return g_date_time_new_from_unix_utc (t);

  return NULL;
}

static gboolean
parse_argument (const gchar *value,
                GDateTime **out,
                GError **error)
{
  if (value == NULL)
    return TRUE;

  *out = parse_time (value);
  if (*out == NULL)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   "Failed to parse %s", value);
      return FALSE;
    }

  return TRUE;
}

static gboolean
arg_start_cb (const gchar *option_name,
              const gchar *value,
              gpointer data,
              GError **error)
{
  update_start = TRUE;

  return parse_argument (value, &start_date, error);
}

static gboolean
arg_end_cb (const gchar *option_name,
            const gchar *value,
            gpointer data,
            GError **error)
{
  update_end = TRUE;

  return parse_argument (value, &end_date, error);
}

int
main (int argc,
      char **argv)
{
  GOptionContext *context;
  g_autoptr (GError) error = NULL;
  const GOptionEntry entries[] =
      {
        { "start-time", 's', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, arg_start_cb,
          "Update the start time of the journey", NULL },
        { "estimated-end-time", 'e', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, arg_end_cb,
          "Update the estimated time of the journey", NULL },
        {
            NULL,
        },
      };
  g_autoptr (GDBusConnection) conn = NULL;

  context = g_option_context_new ("- Update journey progress information");
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_set_summary (context, "Each command takes an optional TIME argument which can be:\n"
                                         "  now : the current date and time\n"
                                         "  now+SECONDS : the current date and time with the specified number of seconds added\n"
                                         "  now-SECONDS : the current date and time with the specified number of seconds substracted\n"
                                         "  TIMESTAMP : a UNIX timestamp\n"
                                         "  if not specified the start or estimated end time is reset");
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_print ("Option parsing failed: %s\n", error->message);
      return -1;
    }

  if (!update_start && !update_end)
    {
      g_autofree gchar *help = NULL;

      help = g_option_context_get_help (context, FALSE, NULL);
      g_print ("%s", help);
      return -1;
    }

  conn = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
  if (conn == NULL)
    {
      g_warning ("Failed to connect to D-Bus: %s", error->message);
      return -1;
    }

  trp_client_mock_new_async (conn, NULL, mock_cb, NULL);

  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);

  g_main_loop_unref (loop);
  g_clear_pointer (&start_date, g_date_time_unref);
  g_clear_pointer (&end_date, g_date_time_unref);
  return success ? 0 : -1;
}
