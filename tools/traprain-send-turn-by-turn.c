/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include <traprain-service/turn-by-turn-notification.h>

gboolean success = TRUE;

static void
send_cb (GObject *source,
         GAsyncResult *result,
         gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  GMainLoop *loop = user_data;

  if (!trp_service_turn_by_turn_notification_send_finish (TRP_SERVICE_TURN_BY_TURN_NOTIFICATION (source),
                                                          result, &error))
    {
      g_warning ("Failed to send notification: %s", error->message);
      success = FALSE;
    }

  g_main_loop_quit (loop);
}

int
main (int argc,
      char **argv)
{
  GOptionContext *context;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *summary = NULL;
  g_autofree gchar *body = NULL;
  g_autofree gchar *icon = NULL;
  gint timeout = -1;
  const GOptionEntry entries[] =
      {
        { "summary", 's', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &summary,
          "The summary text of the notification", NULL },
        { "body", 'b', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &summary,
          "The body of the notification", NULL },
        { "icon", 'i', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &icon,
          "The icon of the notification", NULL },
        { "timeout", 't', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &timeout,
          "The timeout of the notification, in milliseconds", NULL },
        {
            NULL,
        },
      };
  g_autoptr (GMainLoop) loop = NULL;
  g_autoptr (GDBusConnection) conn = NULL;
  g_autoptr (TrpServiceTurnByTurnNotification) notification = NULL;

  context = g_option_context_new ("- Send turn-by-turn notification");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_print ("Option parsing failed: %s\n", error->message);
      return -1;
    }

  if (summary == NULL)
    {
      g_print ("Summary field is mandatory\n");
      return -1;
    }

  conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  if (conn == NULL)
    {
      g_warning ("Failed to connect to D-Bus: %s", error->message);
      return -1;
    }

  notification = trp_service_turn_by_turn_notification_new (conn, summary, body, icon);

  loop = g_main_loop_new (NULL, FALSE);
  trp_service_turn_by_turn_notification_send_async (notification, timeout, send_cb, loop);
  g_main_loop_run (loop);

  return success ? 0 : -1;
}
