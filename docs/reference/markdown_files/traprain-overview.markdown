## Traprain Overview

[Mail the maintainers](mailto:traprain@apertis.org)

Traprain is a set of libraries allowing navigation services, such as a car
GPS routing application, to share navigation and routing information to third
party applications.

The overall design of the navigation system is given in the [Geolocation and navigation design document](https://docs.apertis.org/design/geolocation-and-navigation.html).

### Service side API

Navigation services should use the *traprain-service* library to share routing information with
external applications.

This is done by creating a [TrpServiceNavigation](traprain-service/navigation.h) object.
Once initialized, it will take care to claim the
`org.apertis.Navigation1` D-Bus well-known name and expose the
`/org/apertis/Navigation1/Routes` object on the bus.

They should then use its API to publish potential navigation routes,
represented as [TrpRoute](traprain-common/route.h) objects.

They can also create a [TrpServiceNavigationGuidanceProgress](traprain-service/navigation-guidance-progress.h)
object to publish information about the current journey, such as its start and estimated end time.
Once initialized, it will take care to claim the
`org.apertis.NavigationGuidance1.Progress` D-Bus well-known name and expose the
`/org/apertis/NavigationGuidance1/Progress` object on the bus.

System services can use the [TrpServiceTurnByTurnNotification](traprain-service/turn-by-turn-notification.h)
API to send turn-by-turn notifications to the UI responsible of displaying them.

### Client side API

SDK applications willing to retrieve the routing information shared by the
navigation system have to use to *traprain-client* library.

They should create and initialize a [TrpClientNavigation](traprain-client/navigation.h) object
which will take care of communicating with the navigation service using D-Bus.
Each potential navigation route will be exposed by this object using
[TrpClientRoute](TrpClientRoute) objects.

In order to retrieve information about the current journey, they should create and initialize a
[TrpClientNavigationGuidanceProgress](traprain-client/navigation-guidance-progress.h) object
which will take care of communicating with the navigation service using D-Bus.

### Guidance API

The UI responsible of displaying turn-by-turn notifications has to use the
*traprain-guidance* library.

They should create and initialize a [TrpGuidanceTurnByTurnService](traprain-guidance/turn-by-turn-notification.h) object
which will take care of communicating with the navigation service using D-Bus.
Once initialized, it will take care to claim the
`org.apertis.NavigationGuidance1.TurnByTurn` D-Bus well-known name and expose the
`/org/apertis/NavigationGuidance1/TurnByTurn` object on the bus.

Notifications received for displaying from the navigation system will be represented as
[TrpGuidanceTurnByTurnNotification](traprain-guidance/turn-by-turn-notification.h) objects.

### Mock service

In order to allow developers to test their code using the SDK emulator,
Traprain also provides a mock implementation of the navigation service.
This service can be easily controlled using the [TrpClientMock](traprain-client/mock.h) API.
