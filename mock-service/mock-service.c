/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "mock-service.h"

#include <gio/gio.h>
#include <stdio.h>
#include <sys/types.h>
#include <systemd/sd-daemon.h>
#include <unistd.h>

#include "dbus/org.apertis.Traprain1.Mock.h"
#include "traprain-common/route-internal.h"
#include "traprain-service/navigation-guidance-progress.h"
#include "traprain-service/navigation.h"

#define MOCK_BUS_NAME "org.apertis.Traprain1.Mock"
#define NAVIGATION_ROUTES_PATH "/org/apertis/Navigation1/Routes"

struct _TrpMockService
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  TrpServiceNavigation *service;
  TrpServiceNavigationGuidanceProgress *progress;
  Trp1Mock *mock;
  GCancellable *cancellable;

  GError *run_error;
  gboolean run_exited;
  guint own_name_id;
};

G_DEFINE_TYPE (TrpMockService, trp_mock_service, G_TYPE_OBJECT)

static void
trp_mock_service_dispose (GObject *object)
{
  TrpMockService *self = (TrpMockService *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  g_clear_object (&self->service);
  g_clear_object (&self->progress);
  g_clear_object (&self->mock);
  g_clear_object (&self->conn);

  G_OBJECT_CLASS (trp_mock_service_parent_class)
      ->dispose (object);
}

static void
trp_mock_service_class_init (TrpMockServiceClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = trp_mock_service_dispose;
}

static void
route_set_title (TrpRoute *route,
                 GVariant *title)
{
  g_autoptr (GVariantIter) iter = NULL;
  const gchar *lang, *str;

  g_variant_get (title, "a{ss}", &iter);
  while (g_variant_iter_next (iter, "{&s&s}", &lang, &str))
    trp_route_add_title (route, lang, str);
}

static gboolean
route_set_segments (TrpRoute *route,
                    GVariant *geometry,
                    GError **error)
{
  g_autoptr (GVariantIter) iter = NULL;
  gdouble lat, lon;

  if (g_variant_n_children (geometry) < 2)
    {
      g_set_error_literal (error, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS,
                           "Route should have at least 2 segments");
      return FALSE;
    }

  g_variant_get (geometry, "a(dd)", &iter);
  while (g_variant_iter_next (iter, "(dd)", &lat, &lon))
    {
      if (lat < -90.0 || lat > 90.0)
        {
          g_set_error_literal (error, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS,
                               "Latitude should be between -90.0 and 90.0");
          return FALSE;
        }

      if (lon < -180.0 || lon > 180.0)
        {
          g_set_error_literal (error, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS,
                               "Longitude should be between -180.0 and 180.0");
          return FALSE;
        }

      trp_route_add_segment (route, lat, lon);
    }

  return TRUE;
}

static void
route_set_segments_desc (TrpRoute *route,
                         GVariant *geometry_desc)
{
  g_autoptr (GVariantIter) iter = NULL;
  GVariantIter *iter_desc;
  guint i;

  g_variant_get (geometry_desc, "a{ua{ss}}", &iter);
  while (g_variant_iter_next (iter, "{ua{ss}}", &i, &iter_desc))
    {
      const gchar *lang, *str;

      while (g_variant_iter_next (iter_desc, "{&s&s}", &lang, &str))
        trp_route_add_segment_description (route, i, lang, str);
      g_variant_iter_free (iter_desc);
    }
}

static gboolean
on_handle_add_route (Trp1Mock *mock,
                     GDBusMethodInvocation *invocation,
                     GVariant *title,
                     guint total_distance,
                     guint total_time,
                     GVariant *geometry,
                     GVariant *geometry_desc,
                     gpointer user_data)
{
  TrpMockService *self = user_data;
  TrpRoute *route;
  gint res;
  GError *error = NULL;

  route = trp_service_navigation_create_route (self->service, total_distance, total_time);

  route_set_title (route, title);
  if (!route_set_segments (route, geometry, &error))
    goto error;
  route_set_segments_desc (route, geometry_desc);

  res = trp_service_navigation_add_route (self->service, route, -1, &error);
  if (res == -1)
    goto error;

  trp_1_mock_complete_add_route (mock, invocation, res);
  return TRUE;

error:
  g_dbus_method_invocation_take_error (invocation, error);
  return TRUE;
}

static gboolean
on_handle_remove_route (Trp1Mock *mock,
                        GDBusMethodInvocation *invocation,
                        guint index,
                        gpointer user_data)
{
  TrpMockService *self = user_data;
  GError *error = NULL;
  guint n_routes;

  n_routes = trp_service_navigation_get_n_routes (self->service);
  if (index >= n_routes)
    {
      g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "Invalid index, service contains only %d routes",
                                             n_routes);
      return TRUE;
    }

  if (!trp_service_navigation_remove_route (self->service, index, &error))
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return TRUE;
    }

  trp_1_mock_complete_remove_route (mock, invocation);
  return TRUE;
}

static gboolean
on_handle_set_current_route (Trp1Mock *mock,
                             GDBusMethodInvocation *invocation,
                             guint index,
                             gpointer user_data)
{
  TrpMockService *self = user_data;
  GError *error = NULL;
  guint n_routes;

  n_routes = trp_service_navigation_get_n_routes (self->service);
  if (index >= n_routes)
    {
      g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "Invalid index, service contains only %d routes",
                                             n_routes);
      return TRUE;
    }

  if (!trp_service_navigation_set_current_route (self->service, index, &error))
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return TRUE;
    }

  trp_1_mock_complete_set_current_route (mock, invocation);
  return TRUE;
}

static gboolean
on_handle_clear_routes (Trp1Mock *mock,
                        GDBusMethodInvocation *invocation,
                        gpointer user_data)
{
  TrpMockService *self = user_data;
  GError *error = NULL;
  GPtrArray *routes;

  routes = g_ptr_array_new_with_free_func (g_object_unref);

  if (!trp_service_navigation_set_routes (self->service, routes, &error))
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return TRUE;
    }

  if (!trp_service_navigation_set_current_route (self->service, -1, &error))
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return TRUE;
    }

  trp_1_mock_complete_set_current_route (mock, invocation);
  return TRUE;
}

static gboolean
on_handle_set_start_time (Trp1Mock *mock,
                          GDBusMethodInvocation *invocation,
                          guint64 ts,
                          gpointer user_data)
{
  TrpMockService *self = user_data;
  GError *error = NULL;
  g_autoptr (GDateTime) dt = NULL;

  if (ts != 0)
    dt = g_date_time_new_from_unix_utc (ts);

  if (!trp_service_navigation_guidance_progress_set_start_time (self->progress,
                                                                dt, &error))
    g_dbus_method_invocation_take_error (invocation, error);
  else
    trp_1_mock_complete_set_start_time (mock, invocation);

  return TRUE;
}

static gboolean
on_handle_set_estimated_end_time (Trp1Mock *mock,
                                  GDBusMethodInvocation *invocation,
                                  guint64 ts,
                                  gpointer user_data)
{
  TrpMockService *self = user_data;
  GError *error = NULL;
  g_autoptr (GDateTime) dt = NULL;

  if (ts != 0)
    dt = g_date_time_new_from_unix_utc (ts);

  if (!trp_service_navigation_guidance_progress_set_estimated_end_time (self->progress,
                                                                        dt, &error))
    g_dbus_method_invocation_take_error (invocation, error);
  else
    trp_1_mock_complete_set_estimated_end_time (mock, invocation);

  return TRUE;
}

static void
trp_mock_service_init (TrpMockService *self)
{
  self->cancellable = g_cancellable_new ();

  self->mock = trp_1_mock_skeleton_new ();

  g_signal_connect (self->mock, "handle-add-route",
                    G_CALLBACK (on_handle_add_route), self);
  g_signal_connect (self->mock, "handle-remove-route",
                    G_CALLBACK (on_handle_remove_route), self);
  g_signal_connect (self->mock, "handle-set-current-route",
                    G_CALLBACK (on_handle_set_current_route), self);
  g_signal_connect (self->mock, "handle-clear-routes",
                    G_CALLBACK (on_handle_clear_routes), self);
  g_signal_connect (self->mock, "handle-set-start-time",
                    G_CALLBACK (on_handle_set_start_time), self);
  g_signal_connect (self->mock, "handle-set-estimated-end-time",
                    G_CALLBACK (on_handle_set_estimated_end_time), self);
}

TrpMockService *
trp_mock_service_new (void)
{
  return g_object_new (TRP_MOCK_TYPE_SERVICE, NULL);
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  TrpMockService *self = user_data;

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->mock),
                                    conn, NAVIGATION_ROUTES_PATH,
                                    &self->run_error);

  /* Notify systemd we’re ready. */
  sd_notify (0, "READY=1");
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  TrpMockService *self = user_data;

  self->run_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER,
                                 "Lost bus name '%s'", name);
}

static void
progress_init_cb (GObject *source,
                  GAsyncResult *result,
                  gpointer user_data)
{
  TrpMockService *self = user_data;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (source), result,
                                     &self->run_error))
    return;

  self->own_name_id = g_bus_own_name_on_connection (self->conn, MOCK_BUS_NAME,
                                                    G_BUS_NAME_OWNER_FLAGS_NONE, bus_name_acquired_cb,
                                                    bus_name_lost_cb, self, NULL);
}

static void
service_init_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  TrpMockService *self = user_data;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (source), result,
                                     &self->run_error))
    return;

  /* Now init the Progress service */
  self->progress = trp_service_navigation_guidance_progress_new (self->conn);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->progress),
                               G_PRIORITY_DEFAULT, self->cancellable,
                               progress_init_cb, self);
}

static void
bus_get_cb (GObject *source,
            GAsyncResult *result,
            gpointer user_data)
{
  TrpMockService *self = user_data;

  self->conn = g_bus_get_finish (result, &self->run_error);
  if (self->conn == NULL)
    return;

  self->service = trp_service_navigation_new (self->conn);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->service),
                               G_PRIORITY_DEFAULT, self->cancellable,
                               service_init_cb, self);
}

void
trp_mock_service_run (TrpMockService *self,
                      gboolean use_session_bus,
                      GError **error)
{
  /* Ensure we are not running as root — we don’t need those privileges. */
  if (getuid () == 0 || geteuid () == 0)
    {
      g_set_error_literal (error, G_IO_ERROR,
                           G_IO_ERROR_PERMISSION_DENIED,
                           "This daemon must not be run as root.");
      return;
    }

  g_debug ("Use %s bus", use_session_bus ? "session" : "system");

  g_bus_get (use_session_bus ? G_BUS_TYPE_SESSION : G_BUS_TYPE_SYSTEM,
             self->cancellable, bus_get_cb, self);

  while (self->run_error == NULL && !self->run_exited)
    g_main_context_iteration (NULL, TRUE);

  /* Notify systemd we’re shutting down. */
  sd_notify (0, "STOPPING=1");

  if (self->run_error != NULL)
    {
      g_propagate_error (error, self->run_error);
      self->run_error = NULL;
      return;
    }
}
