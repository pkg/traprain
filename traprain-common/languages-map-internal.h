/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_LANGUAGES_MAP_INTERNAL_H__
#define __TRAPRAIN_LANGUAGES_MAP_INTERNAL_H__

#include <glib.h>

#include "languages-map.h"

G_BEGIN_DECLS

TrpLanguagesMap *_trp_languages_map_new (void);

#define _trp_languages_map_unref g_array_unref

TrpLanguagesMap *_trp_languages_map_new_from_variant (GVariant *v);

void _trp_languages_map_add (TrpLanguagesMap *map,
                             const gchar *language,
                             const gchar *string);

GVariant *_trp_languages_map_to_variant (TrpLanguagesMap *map);

void _trp_languages_map_copy_content (TrpLanguagesMap *dest,
                                      TrpLanguagesMap *source);

void _trp_languages_map_assert_same_content (TrpLanguagesMap *a,
                                             TrpLanguagesMap *b);

G_END_DECLS

#endif /* __TRAPRAIN_LANGUAGES_MAP_INTERNAL_H__ */
