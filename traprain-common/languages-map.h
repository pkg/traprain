/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_LANGUAGES_MAP_H__
#define __TRAPRAIN_LANGUAGES_MAP_H__

#include <glib.h>

G_BEGIN_DECLS

typedef GArray /*<owned _TrpLanguageString>*/ TrpLanguagesMap;

const gchar *trp_languages_map_lookup (TrpLanguagesMap *map,
                                       const gchar *language);

gboolean trp_languages_map_get (TrpLanguagesMap *map,
                                gsize index,
                                const gchar **language,
                                const gchar **string);

G_END_DECLS

#endif /* __TRAPRAIN_LANGUAGES_MAP__H__ */
