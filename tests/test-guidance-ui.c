/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* Test SDK traprain-guidance-ui. We can't check if notifications
 * are actually displayed but can at least ensure the service isn't crashing */

#include <gio/gio.h>
#include <glib.h>

#include "dbus/org.apertis.NavigationGuidance1.TurnByTurn.h"
#include "traprain-service/turn-by-turn-notification.h"

#define TBT_BUS_NAME "org.apertis.NavigationGuidance1.TurnByTurn"
#define TBT_PATH "/org/apertis/NavigationGuidance1/TurnByTurn"
#define TBT_IFACE "org.apertis.NavigationGuidance1.TurnByTurn"

#define APP_BUS_NAME "org.apertis.Traprain1.GuidanceUI"
#define APP_PATH "/org/apertis/Traprain1/GuidanceUI"
#define APP_IFACE "org.freedesktop.Application"

typedef struct
{
  GDBusConnection *conn; /* owned */
  GMainLoop *loop;       /* owned */
  GError *error;         /* owned */
  gboolean result;
  guint wait;
} Test;

static void
setup (Test *test,
       gconstpointer unused)
{
  test->conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &test->error);

  test->loop = g_main_loop_new (NULL, FALSE);
  test->error = NULL;
}

static void
teardown (Test *test,
          gconstpointer unused)
{
  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  g_clear_object (&test->conn);
}

static void
create_proxy (Test *test,
              const gchar *bus_name,
              const gchar *path,
              const gchar *iface)
{
  g_autofree gchar *owner = NULL;
  g_autoptr (GDBusProxy) proxy = NULL;

  proxy = g_dbus_proxy_new_sync (test->conn, G_DBUS_PROXY_FLAGS_NONE, NULL,
                                 bus_name, path, iface, NULL, &test->error);

  g_assert_no_error (test->error);
  g_assert (G_IS_DBUS_PROXY (proxy));
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (proxy));
  g_assert (owner != NULL);
}

/* Call Ping() to check if the service is running */
static void
check_service_is_running (Test *test)
{
  g_autoptr (GVariant) result = NULL;

  result = g_dbus_connection_call_sync (test->conn, TBT_BUS_NAME, TBT_PATH,
                                        "org.freedesktop.DBus.Peer", "Ping", NULL,
                                        NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                                        &test->error);
  g_assert_no_error (test->error);
}

/* Test invocation of the service using the navigation bus name */
static void
test_invocation_navigation (Test *test,
                            gconstpointer unused)
{
  create_proxy (test, TBT_BUS_NAME, TBT_PATH, TBT_IFACE);
  check_service_is_running (test);
}

/* Test invocation of the service using the navigation bus name */
static void
test_invocation_application (Test *test,
                             gconstpointer unused)
{
  create_proxy (test, APP_BUS_NAME, APP_PATH, APP_IFACE);
  check_service_is_running (test);
}

static void
send_cb (GObject *source,
         GAsyncResult *result,
         gpointer user_data)
{
  TrpServiceTurnByTurnNotification *notification = TRP_SERVICE_TURN_BY_TURN_NOTIFICATION (source);
  Test *test = user_data;

  g_clear_error (&test->error);
  test->result = trp_service_turn_by_turn_notification_send_finish (notification, result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
send_notification (Test *test,
                   TrpServiceTurnByTurnNotification *notification,
                   gint timeout)
{
  g_assert (TRP_SERVICE_IS_TURN_BY_TURN_NOTIFICATION (notification));

  test->wait = 1;
  trp_service_turn_by_turn_notification_send_async (notification, timeout, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);
  check_service_is_running (test);
}

static void
close_cb (GObject *source,
          GAsyncResult *result,
          gpointer user_data)
{
  TrpServiceTurnByTurnNotification *notification = TRP_SERVICE_TURN_BY_TURN_NOTIFICATION (source);
  Test *test = user_data;

  g_clear_error (&test->error);
  test->result = trp_service_turn_by_turn_notification_close_finish (notification, result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
close_notification (Test *test,
                    TrpServiceTurnByTurnNotification *notification)
{
  g_assert (TRP_SERVICE_IS_TURN_BY_TURN_NOTIFICATION (notification));

  test->wait = 1;
  trp_service_turn_by_turn_notification_close_async (notification, close_cb, test);
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);
  check_service_is_running (test);
}

/* Test sending and closing notifications */
static void
test_notification_send (Test *test,
                        gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) first = NULL, second = NULL;

  first = trp_service_turn_by_turn_notification_new (test->conn, "Turn Left",
                                                     "Take the second left turn in 100 meters", "firefox");
  send_notification (test, first, -1);

  second = trp_service_turn_by_turn_notification_new (test->conn, "Turn Right", NULL, NULL);
  send_notification (test, second, -1);

  close_notification (test, first);
  close_notification (test, second);
}

/* Send a notification and wait it expires */
static void
test_notification_expire (Test *test,
                          gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) notification;
  const gint timer = 500; /* milliseconds */

  notification = trp_service_turn_by_turn_notification_new (test->conn, "Turn Left",
                                                            "Take the second left turn in 100 meters", NULL);
  send_notification (test, notification, timer);
  sleep (1); /* Wait twice the timer just to be sure */
  check_service_is_running (test);

  /* Update the notification and then close it */
  trp_service_turn_by_turn_notification_set_summary (notification, "Take the second left turn in 50 meters");
  send_notification (test, notification, timer);
  close_notification (test, notification);
}

/* Update a notification which never expires */
static void
test_notification_update (Test *test,
                          gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) notification;

  notification = trp_service_turn_by_turn_notification_new (test->conn, "Turn Left",
                                                            "Take the second left turn in 100 meters", NULL);
  send_notification (test, notification, 0);

  /* Update the notification and then close it */
  trp_service_turn_by_turn_notification_set_summary (notification, "Take the second left turn in 50 meters");
  send_notification (test, notification, 0);
  close_notification (test, notification);

  /* Try updating again now it's been closed */
  trp_service_turn_by_turn_notification_set_summary (notification, "Take the second left turn in 10 meters");
  send_notification (test, notification, 1000);
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/guidance-ui/invocation/navigation", Test, NULL,
              setup, test_invocation_navigation, teardown);
  g_test_add ("/guidance-ui/invocation/application", Test, NULL,
              setup, test_invocation_application, teardown);
  g_test_add ("/guidance-ui/notification/send", Test, NULL,
              setup, test_notification_send, teardown);
  g_test_add ("/guidance-ui/notification/expire", Test, NULL,
              setup, test_notification_expire, teardown);
  g_test_add ("/guidance-ui/notification/update", Test, NULL,
              setup, test_notification_update, teardown);

  return g_test_run ();
}
