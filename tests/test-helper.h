/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "traprain-client/route.h"
#include "traprain-service/navigation.h"

#ifndef __TRAPRAIN_TEST_HELPER_H__
#define __TRAPRAIN_TEST_HELPER_H__

G_BEGIN_DECLS

/* Proposed to GLib: https://bugzilla.gnome.org/show_bug.cgi?id=769928 */
#define assert_equal_variants(v1, v2)                                              \
  G_STMT_START                                                                     \
  {                                                                                \
    GVariant *__v1 = (v1), *__v2 = (v2);                                           \
    if (!g_variant_equal (__v1, __v2))                                             \
      {                                                                            \
        gchar *__s1, *__s2, *__msg;                                                \
        __s1 = g_variant_print (__v1, TRUE);                                       \
        __s2 = g_variant_print (__v2, TRUE);                                       \
        __msg = g_strdup_printf (#v1 " %s does not equal " #v2 " %s", __s1, __s2); \
        g_assertion_message (G_LOG_DOMAIN, __FILE__, __LINE__, G_STRFUNC, __msg);  \
        g_free (__s1);                                                             \
        g_free (__s2);                                                             \
        g_free (__msg);                                                            \
      }                                                                            \
  }                                                                                \
  G_STMT_END

#define FST_ROUTE_DISTANCE 10
#define FST_ROUTE_TIME 15
#define FST_ROUTE_TITLE_EN "First Route"
#define FST_ROUTE_TITLE_FR "Première route"
#define FST_ROUTE_FST_SEG_LAT 10
#define FST_ROUTE_FST_SEG_LON 20
#define FST_ROUTE_FST_SEG_DESC_EN "First route, first segment description"
#define FST_ROUTE_FST_SEG_DESC_FR "Première route, premier segment description"
#define FST_ROUTE_SND_SEG_LAT 30
#define FST_ROUTE_SND_SEG_LON 40
#define FST_ROUTE_SND_SEG_DESC_EN "First route, second segment description"

#define SND_ROUTE_DISTANCE 100
#define SND_ROUTE_TIME 150
#define SND_ROUTE_TITLE_EN "Second Route"
#define SND_ROUTE_TITLE_FR "Deuxième route"
#define SND_ROUTE_FST_SEG_LAT -10.5
#define SND_ROUTE_FST_SEG_LON 20.5
#define SND_ROUTE_FST_SEG_DESC_FR "Seconde route, second segment, description"
#define SND_ROUTE_SND_SEG_LAT -21
#define SND_ROUTE_SND_SEG_LON 40

#define THD_ROUTE_DISTANCE 200
#define THD_ROUTE_TIME 250
#define THD_ROUTE_TITLE_EN "Third Route"
#define THD_ROUTE_FST_SEG_LAT 40.5
#define THD_ROUTE_FST_SEG_LON -100.5
#define THD_ROUTE_FST_SEG_DESC_FR "Troisème route, premiet segment, description"
#define THD_ROUTE_SND_SEG_LAT -30
#define THD_ROUTE_SND_SEG_LON -145

TrpRoute *create_first_service_route (TrpServiceNavigation *nav);
TrpRoute *create_second_service_route (TrpServiceNavigation *nav);
TrpRoute *create_third_service_route (TrpServiceNavigation *nav);

void build_first_route (TrpRoute *route);
void build_second_route (TrpRoute *route);
void build_third_route (TrpRoute *route);

void check_first_client_route (TrpClientRoute *route);
void check_second_client_route (TrpClientRoute *route);
void check_third_client_route (TrpClientRoute *route);

G_END_DECLS

#endif /* __TRAPRAIN_TEST_HELPER_H__ */
