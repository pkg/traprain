/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdlib.h>

#include <gio/gio.h>
#include <glib.h>

#include "traprain-common/place.h"

/* Put this one first, before we turn off -Wfloat-equal, to
 * check that we can use these macros without triggering warnings. */

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic error "-Wfloat-equal"
#endif

static void
test_macros (void)
{
  g_assert_false (trp_place_is_latitude (+90.5));
  g_assert_true (trp_place_is_latitude (+90.0));
  g_assert_true (trp_place_is_latitude (0.0));
  g_assert_true (trp_place_is_latitude (-90.0));
  g_assert_false (trp_place_is_latitude (-90.5));
  g_assert_false (trp_place_is_latitude (TRP_PLACE_LATITUDE_UNKNOWN));

  g_assert_false (trp_place_is_longitude (+180.5));
  g_assert_true (trp_place_is_longitude (+180.0));
  g_assert_true (trp_place_is_longitude (0.0));
  g_assert_true (trp_place_is_longitude (-180.0));
  g_assert_false (trp_place_is_longitude (-180.5));
  g_assert_false (trp_place_is_longitude (TRP_PLACE_LONGITUDE_UNKNOWN));

  g_assert_true (trp_place_is_altitude (10000.0));
  g_assert_true (trp_place_is_altitude (0.0));
  g_assert_true (trp_place_is_altitude (-100.0));
  g_assert_false (trp_place_is_altitude (-1e8));
  g_assert_false (trp_place_is_altitude (TRP_PLACE_ALTITUDE_UNKNOWN));

  g_assert_true (trp_place_is_uncertainty (10000.0));
  g_assert_true (trp_place_is_uncertainty (1.0));
  g_assert_true (trp_place_is_uncertainty (0.0));
  g_assert_false (trp_place_is_uncertainty (-1.0));
  g_assert_false (trp_place_is_uncertainty (TRP_PLACE_UNCERTAINTY_UNKNOWN));
}

/* We are using float equality in the rest of this test; don't warn about it,
 * since we are only using it for quantities that should be stored without
 * any parsing or computation, or that can be represented exactly in
 * text. */
#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic ignored "-Wfloat-equal"
#endif

static void
test_builder (void)
{
  g_autoptr (TrpPlace) place = NULL;
  g_autoptr (TrpPlaceBuilder) builder = NULL;
  g_autofree gchar *location_string = NULL;

  builder = trp_place_builder_new (NULL);

  /* This pair of statements is refcount-neutral. g_assert_true() is
   * always compiled in */
  g_assert_true (trp_place_builder_ref (builder) == builder);
  trp_place_builder_unref (builder);

  trp_place_builder_set_latitude (builder, 50.827138);
  trp_place_builder_set_longitude (builder, -0.230885);

  place = trp_place_builder_copy (builder);
  g_assert_cmpfloat (trp_place_get_latitude (place), ==, 50.827138);
  g_assert_cmpfloat (trp_place_get_longitude (place), ==, -0.230885);
  g_assert_cmpfloat (trp_place_get_altitude (place), ==,
                     TRP_PLACE_ALTITUDE_UNKNOWN);
  g_assert_cmpfloat (trp_place_get_uncertainty (place), ==,
                     TRP_PLACE_UNCERTAINTY_UNKNOWN);
  g_assert_cmpint (trp_place_get_crs (place), ==,
                   TRP_COORDINATE_REFERENCE_SYSTEM_WGS84);
  g_assert_cmpstr (trp_place_get_location_string (place), ==, NULL);
  location_string = trp_place_force_location_string (place);
  g_assert_cmpstr (location_string, ==, "50.83°N 0.23°W");
  g_clear_pointer (&location_string, g_free);

  trp_place_builder_set_altitude (builder, 14.0);
  trp_place_builder_set_uncertainty (builder, 100.0);
  trp_place_builder_set_location_string (builder, "Shoreham Port");
  /* it didn't affect the existing object */
  g_assert_cmpstr (trp_place_get_location_string (place), ==, NULL);
  g_assert_cmpfloat (trp_place_get_uncertainty (place), ==,
                     TRP_PLACE_UNCERTAINTY_UNKNOWN);

  g_clear_object (&place);
  place = trp_place_builder_end (builder);
  g_assert_cmpfloat (trp_place_get_latitude (place), ==, 50.827138);
  g_assert_cmpfloat (trp_place_get_longitude (place), ==, -0.230885);
  g_assert_cmpfloat (trp_place_get_uncertainty (place), ==, 100.0);
  g_assert_cmpfloat (trp_place_get_altitude (place), ==, 14.0);
  g_assert_cmpint (trp_place_get_crs (place), ==,
                   TRP_COORDINATE_REFERENCE_SYSTEM_WGS84);
  g_assert_cmpstr (trp_place_get_location_string (place), ==, "Shoreham Port");
  location_string = trp_place_force_location_string (place);
  g_assert_cmpstr (location_string, ==, "Shoreham Port");
  g_clear_pointer (&location_string, g_free);

  g_clear_object (&place);
  /* After being ended, the builder is blanked, but it is not allowed to end a
   * blank builder, it needs at least location string or lat/long pair */
  trp_place_builder_set_location_string (builder, "Nowhere");
  place = trp_place_builder_end (builder);
  g_assert_false (trp_place_is_latitude (trp_place_get_latitude (place)));
  g_assert_false (trp_place_is_longitude (trp_place_get_longitude (place)));
  g_assert_false (trp_place_is_altitude (trp_place_get_altitude (place)));
  g_assert_false (trp_place_is_uncertainty (trp_place_get_uncertainty (place)));
  g_assert_cmpint (trp_place_get_crs (place), ==,
                   TRP_COORDINATE_REFERENCE_SYSTEM_WGS84);
  g_assert_cmpstr (trp_place_get_location_string (place), ==, "Nowhere");

  trp_place_builder_set_location_string (builder, NULL);
  trp_place_builder_set_area (builder, "Southwick");
  trp_place_builder_set_locality (builder, "Brighton");
  trp_place_builder_set_region (builder, "Sussex");
  trp_place_builder_set_postal_code (builder, "BN42 4ED");
  trp_place_builder_set_street (builder, "Albion Street");
  trp_place_builder_set_building (builder, "Nautilus House");
  trp_place_builder_set_country_code (builder, "GB");
  trp_place_builder_set_formatted_address (builder,
                                           "Shoreham Port\n"
                                           "Nautilus House\n"
                                           "90-100 Albion Street\n"
                                           "Southwick\n"
                                           "Brighton\n"
                                           "BN42 4ED\n");
  place = trp_place_builder_copy (builder);
  location_string = trp_place_force_location_string (place);
  g_assert_cmpstr (location_string, ==,
                   trp_place_get_formatted_address (place));
  g_assert_cmpstr (location_string, ==, "Shoreham Port\n"
                                        "Nautilus House\n"
                                        "90-100 Albion Street\n"
                                        "Southwick\n"
                                        "Brighton\n"
                                        "BN42 4ED\n");
  g_assert_cmpstr (trp_place_get_postal_code (place), ==, "BN42 4ED");
  g_assert_cmpstr (trp_place_get_building (place), ==, "Nautilus House");
  g_assert_cmpstr (trp_place_get_street (place), ==, "Albion Street");
  g_assert_cmpstr (trp_place_get_area (place), ==, "Southwick");
  g_assert_cmpstr (trp_place_get_locality (place), ==, "Brighton");
  g_assert_cmpstr (trp_place_get_region (place), ==, "Sussex");
  g_assert_cmpstr (trp_place_get_country_code (place), ==, "GB");
  g_clear_pointer (&location_string, g_free);
  g_clear_object (&place);

  trp_place_builder_set_location_string (builder, "Shoreham Port");
  trp_place_builder_set_building (builder, "90-100");
  place = trp_place_builder_copy (builder);
  location_string = trp_place_force_location_string (place);
  g_assert_cmpstr (location_string, ==, "Shoreham Port");
  g_assert_cmpstr (trp_place_get_building (place), ==, "90-100");
  g_clear_pointer (&location_string, g_free);
  g_clear_object (&place);

  g_clear_pointer (&builder, trp_place_builder_unref);
  /* In g_object_new() we don't have much opportunity for useful
   * assertions, so we normalize "" to NULL (apart from country)
   * and normalize out-of-range numeric properties to UNKNOWN. */
  place = g_object_new (TRP_TYPE_PLACE,
                        "altitude", -1e8,
                        "area", "",
                        "building", "",
                        "country", NULL,
                        "formatted-address", "",
                        "latitude", -93.0,
                        "locality", "",
                        "location-string", "hello",
                        "longitude", -185.0,
                        "postal-code", "",
                        "region", "",
                        "street", "",
                        "uncertainty", -1.0,
                        NULL);
  g_assert_nonnull (place);
  builder = trp_place_builder_new (place);
  g_clear_object (&place);
  place = trp_place_builder_end (builder);

  g_assert_cmpstr (trp_place_get_location_string (place), ==, "hello");
  g_assert_cmpfloat (trp_place_get_altitude (place), ==,
                     TRP_PLACE_ALTITUDE_UNKNOWN);
  g_assert_cmpfloat (trp_place_get_latitude (place), ==,
                     TRP_PLACE_LATITUDE_UNKNOWN);
  g_assert_cmpfloat (trp_place_get_longitude (place), ==,
                     TRP_PLACE_LATITUDE_UNKNOWN);
  g_assert_cmpfloat (trp_place_get_uncertainty (place), ==,
                     TRP_PLACE_UNCERTAINTY_UNKNOWN);
  g_assert_cmpstr (trp_place_get_postal_code (place), ==, NULL);
  g_assert_cmpstr (trp_place_get_building (place), ==, NULL);
  g_assert_cmpstr (trp_place_get_street (place), ==, NULL);
  g_assert_cmpstr (trp_place_get_area (place), ==, NULL);
  g_assert_cmpstr (trp_place_get_locality (place), ==, NULL);
  g_assert_cmpstr (trp_place_get_region (place), ==, NULL);
  g_assert_cmpstr (trp_place_get_country_code (place), ==, NULL);
}

static void
test_to_uri (void)
{
  g_autoptr (TrpPlace) place = NULL;
  g_autoptr (TrpPlaceBuilder) builder = NULL;
  g_autofree gchar *uri = NULL;
  g_autofree gchar *any_uri = NULL;

  builder = trp_place_builder_new (NULL);

  /* This is chosen to be exactly representable in both decimal and FP */
  trp_place_builder_set_latitude (builder, 50.5);
  trp_place_builder_set_longitude (builder, -0.25);

  place = trp_place_builder_copy (builder);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_GEO);
  g_assert_cmpstr (uri, ==, "geo:50.5,-0.25");
  g_clear_pointer (&uri, g_free);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_PLACE);
  /* We don't have a useful location string, so we use a fallback. %C2%B0
   * is the URL-encoding of the degree sign in UTF-8. */
  g_assert_cmpstr (uri, ==, "place:50.50%C2%B0N%200.25%C2%B0W"
                            ";location=geo:50.5%2C-0.25");
  g_clear_pointer (&uri, g_free);
  /* If there is just a geographical location, ANY is equivalent to GEO */
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (uri, ==, "geo:50.5,-0.25");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&place);

  trp_place_builder_set_postal_code (builder, "OMG WTF BBQ");
  place = trp_place_builder_copy (builder);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_GEO);
  g_assert_cmpstr (uri, ==, "geo:50.5,-0.25");
  g_clear_pointer (&uri, g_free);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_PLACE);
  /* We don't have a useful location string, so we use a fallback. %C2%B0
   * is the URL-encoding of the degree sign in UTF-8. */
  g_assert_cmpstr (uri, ==, "place:50.50%C2%B0N%200.25%C2%B0W"
                            ";location=geo:50.5%2C-0.25"
                            ";postal-code=OMG%20WTF%20BBQ");
  /* There is information beyond the geographic location now, so ANY means
   * PLACE */
  any_uri = trp_place_to_uri (place, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (any_uri, ==, uri);
  g_clear_pointer (&any_uri, g_free);
  g_clear_pointer (&uri, g_free);
  g_clear_object (&place);

  /* This is chosen to be exactly representable in both decimal and FP */
  trp_place_builder_set_altitude (builder, 2.375);
  trp_place_builder_set_uncertainty (builder, 1.0);
  trp_place_builder_set_location_string (builder, "Somewhere");
  /* "" gets normalized to NULL */
  trp_place_builder_set_formatted_address (builder, "");
  trp_place_builder_set_country_code (builder, NULL);
  trp_place_builder_set_region (builder, "");
  trp_place_builder_set_locality (builder, "");
  trp_place_builder_set_area (builder, "");
  trp_place_builder_set_street (builder, "");
  trp_place_builder_set_building (builder, "");
  trp_place_builder_set_postal_code (builder, "");

  place = trp_place_builder_copy (builder);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_GEO);
  g_assert_cmpstr (uri, ==, "geo:50.5,-0.25,2.375;u=1");
  g_clear_pointer (&uri, g_free);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_PLACE);
  g_assert_cmpstr (uri, ==,
                   "place:Somewhere;location=geo:50.5%2C-0.25%2C2.375%3Bu%3D1");
  g_clear_pointer (&uri, g_free);
  /* We have enough information now that a place: URI is considered to be
   * superior to a geo: URI */
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (uri, ==,
                   "place:Somewhere;location=geo:50.5%2C-0.25%2C2.375%3Bu%3D1");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&place);

  g_clear_pointer (&builder, trp_place_builder_unref);
  builder = trp_place_builder_new (NULL);

  trp_place_builder_set_location_string (builder, "Shoreham Port");
  place = trp_place_builder_copy (builder);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_GEO);
  /* There is no geo: URI if there isn't enough information */
  g_assert_cmpstr (uri, ==, NULL);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_PLACE);
  g_assert_cmpstr (uri, ==, "place:Shoreham%20Port");
  any_uri = trp_place_to_uri (place, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (any_uri, ==, uri);
  g_clear_pointer (&any_uri, g_free);
  g_clear_pointer (&uri, g_free);
  g_clear_object (&place);

  trp_place_builder_set_country_code (builder, "gb");
  trp_place_builder_set_area (builder, "Southwick");
  trp_place_builder_set_locality (builder, "Brighton");
  trp_place_builder_set_region (builder, "Sussex");
  trp_place_builder_set_postal_code (builder, "BN42 4ED");
  trp_place_builder_set_street (builder, "Albion Street");
  trp_place_builder_set_building (builder, "90-100");
  trp_place_builder_set_formatted_address (builder,
                                           "Shoreham Port\n"
                                           "Nautilus House\n"
                                           "90-100 Albion Street\n"
                                           "Southwick\n"
                                           "Brighton\n"
                                           "BN42 4ED\n");

  place = trp_place_builder_copy (builder);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_GEO);
  /* There is no geo: URI if there isn't enough information */
  g_assert_cmpstr (uri, ==, NULL);
  uri = trp_place_to_uri (place, TRP_URI_SCHEME_PLACE);
  g_assert_cmpstr (uri, ==,
                   "place:Shoreham%20Port"
                   /* This test relies on an implementation detail: we sort
                    * parameters alphabetically. This is not an API
                    * guarantee, but for any parameters whose order is
                    * irrelevant, it makes sense to do. */
                   ";area=Southwick"
                   ";country=GB"
                   ";formatted-address="
                   "Shoreham%20Port%0A"
                   "Nautilus%20House%0A"
                   "90-100%20Albion%20Street%0A"
                   "Southwick%0A"
                   "Brighton%0A"
                   "BN42%204ED%0A"
                   ";locality=Brighton"
                   ";postal-code=BN42%204ED"
                   ";region=Sussex"
                   ";street=Albion%20Street"
                   ";building=90-100");
  any_uri = trp_place_to_uri (place, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (any_uri, ==, uri);
  g_clear_pointer (&any_uri, g_free);
  g_clear_pointer (&uri, g_free);
  g_clear_object (&place);
}

/* Use the implementation detail that all the unknown values are
 * mathematically equal, which lets us abbreviate good_uris[] significantly */
#define UNKNOWN (-G_MAXDOUBLE)

typedef struct
{
  const char *uri;
  gint code;
  gdouble latitude;
  gdouble longitude;
  gdouble altitude;
  gdouble uncertainty;
  const char *location_string;
  const char *postal_code;
  const char *country_code;
  const char *region;
  const char *locality;
  const char *area;
  const char *street;
  const char *building;
  const char *formatted_address;
} SampleUri;

static const SampleUri good_uris[] =
    {
      { "geo:0,1", 0, 0.0, 1.0, UNKNOWN, UNKNOWN, NULL },
      { "geo:0,1,2", 0, 0.0, 1.0, 2.0, UNKNOWN, NULL },
      { "geo:-0.5,-1.25,-2.125", 0, -0.5, -1.25, -2.125, UNKNOWN, NULL },
      { "geo:-0.5,-1.25,-2.125;crs=wgs84", 0, -0.5, -1.25, -2.125, UNKNOWN, NULL },
      { "geo:-0.5,-1.25,-2.125;u=3.75", 0, -0.5, -1.25, -2.125, 3.75, NULL },
      { "GEO:-0.5,-1.25,-2.125;CRS=WGS84;U=3.75", 0, -0.5, -1.25, -2.125, 3.75, NULL },
      /* We accept and ignore unknown parameters even in strict mode */
      { "geo:-0.5,-1.25,-2.125;crs=wgs84;u=3.75;param=whatever",
        0, -0.5, -1.25, -2.125, 3.75, NULL },
      /* At the poles, we normalize longitude to 0 */
      { "geo:90,123", 0, 90.0, 0.0, UNKNOWN, UNKNOWN, NULL },
      { "geo:-90,123", 0, -90.0, 0.0, UNKNOWN, UNKNOWN, NULL },
      { "place:Hello%20world;location=geo:-0.5%2C-1.25%2C-2.125;u=100",
        0, -0.5, -1.25, -2.125, UNKNOWN, "Hello world" },
      { "place:Hello%20world;location=geo:-0.5%2C-1.25%2C-2.125%3Bu%3D100",
        0, -0.5, -1.25, -2.125, 100.0, "Hello world" },
      { "place:Hello%20world", 0, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Hello world" },
      { "place:X", 0, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "X" },
      { "place:X;x-treasure=buried-here", 0, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "X" },
      { "place:Whatever;postal-code=012345;country=bq;region=Here"
        ";locality=Sometown;area=Somewhere;street=West%20Wallaby%20Street"
        ";building=42;formatted-address=...",
        0, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever",
        "012345", "BQ", "Here", "Sometown", "Somewhere", "West Wallaby Street",
        "42", "..." },
    };

static const struct
{
  const char *uri;
  gint code;
} bad_uris[] =
    {
      { "hello", TRP_PLACE_ERROR_INVALID_URI },
      { "about:blank", TRP_PLACE_ERROR_UNKNOWN_SCHEME },
      { "http://example.com/", TRP_PLACE_ERROR_UNKNOWN_SCHEME },
      { "geo://0,1", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:/0,1", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1,2,3", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:,1", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:,,", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,,2", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1,", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1,;param=foo", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1;crs", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1;crs;param=foo", TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1;crs=edenprime2183", TRP_PLACE_ERROR_UNKNOWN_CRS },
      { "place:", TRP_PLACE_ERROR_INVALID_URI },
      { "place:foo;hello!", TRP_PLACE_ERROR_INVALID_URI },
      { "place:foo;x=%", TRP_PLACE_ERROR_INVALID_URI },
      { "place:foo;x=%1", TRP_PLACE_ERROR_INVALID_URI },
      { "place:foo;x=%1x", TRP_PLACE_ERROR_INVALID_URI },
      { "place:foo;x=%x1", TRP_PLACE_ERROR_INVALID_URI },
      { "place:foo;%20", TRP_PLACE_ERROR_INVALID_URI },
      { "place:;location=geo:0%2C1", TRP_PLACE_ERROR_INVALID_URI },
      /* Not hierarchical */
      { "place://hello", TRP_PLACE_ERROR_INVALID_URI },
      { "place:/hello;location=geo:0%2C1", TRP_PLACE_ERROR_INVALID_URI },
      { "place:hello;location=geo:/0%2C1", TRP_PLACE_ERROR_INVALID_URI },
      { "place:hello;location", TRP_PLACE_ERROR_INVALID_URI },
      { "place:hello;location=", TRP_PLACE_ERROR_INVALID_URI },
      { "place:Whatever;country=G", TRP_PLACE_ERROR_INVALID_COUNTRY_CODE },
      { "place:Whatever;country=826", TRP_PLACE_ERROR_INVALID_COUNTRY_CODE },
      { "place:Whatever;country=United%20Kingdom",
        TRP_PLACE_ERROR_INVALID_COUNTRY_CODE },
    };

static const struct
{
  const char *uri;
  gint code_when_tolerant;
  gint code_when_strict;
} differently_bad_uris[] =
    {
      { "geo:0,1;crs=",
        TRP_PLACE_ERROR_UNKNOWN_CRS,
        TRP_PLACE_ERROR_INVALID_URI },
      { "geo:0,1;crs=;param=foo",
        TRP_PLACE_ERROR_UNKNOWN_CRS,
        TRP_PLACE_ERROR_INVALID_URI },
    };

static const SampleUri ugly_uris[] =
    {
      /* Out of range values are clamped in non-strict mode */
      { "geo:92,100", TRP_PLACE_ERROR_OUT_OF_RANGE,
        90.0, 0.0, UNKNOWN, UNKNOWN, NULL },
      { "geo:-91,-100", TRP_PLACE_ERROR_OUT_OF_RANGE,
        -90.0, 0.0, UNKNOWN, UNKNOWN, NULL },
      { "geo:82,181", TRP_PLACE_ERROR_OUT_OF_RANGE,
        82.0, 180.0, UNKNOWN, UNKNOWN, NULL },
      { "geo:-81,-183", TRP_PLACE_ERROR_OUT_OF_RANGE,
        -81.0, 180.0, UNKNOWN, UNKNOWN, NULL },
      { "geo:-0.5,-1.25,-1000000000", TRP_PLACE_ERROR_OUT_OF_RANGE,
        -0.5, -1.25, UNKNOWN, UNKNOWN, NULL },
      { "geo:-0.5,-1.25,-2.125;u=-3.75", TRP_PLACE_ERROR_INVALID_URI,
        -0.5, -1.25, -2.125, UNKNOWN, NULL },
      /* A poorly designed Google Maps extension used on Android */
      { "geo:0,0?q=52.25,0.125(near+Cambridge)", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 0.0, UNKNOWN, UNKNOWN, NULL },
      /* A Google Maps extension: search for a place near here */
      { "geo:52.25,0.125?q=Golden%20Curry", TRP_PLACE_ERROR_INVALID_URI,
        52.25, 0.125, UNKNOWN, UNKNOWN, NULL },
      /* We expect URIs, not URI-references */
      { "geo:52.25,0.125#Whatever", TRP_PLACE_ERROR_INVALID_URI,
        52.25, 0.125, UNKNOWN, UNKNOWN, NULL },
      /* HTTP-style query string and fragment */
      { "geo:52.25,0.125?a=b&c=d&e#f", TRP_PLACE_ERROR_INVALID_URI,
        52.25, 0.125, UNKNOWN, UNKNOWN, NULL },
      /* Parameters aren't parsed after the ? or #, so this doesn't have
   * an uncertainty of ± 1 metre */
      { "geo:52.25,0.125?a;u=1.0", TRP_PLACE_ERROR_INVALID_URI,
        52.25, 0.125, UNKNOWN, UNKNOWN, NULL },
      /* In strict mode we expect URIs, not IRIs */
      { "geo:0,1;param=føö", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 1.0, UNKNOWN, UNKNOWN, NULL },
      /* Check for trailing ';' */
      { "geo:0,1;", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 1.0, UNKNOWN, UNKNOWN, NULL },
      /* Check for multiple trailing ';' */
      { "geo:0,2;;;", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 2.0, UNKNOWN, UNKNOWN, NULL },
      /* The ';' can still be used as a separator */
      { "geo:0,4;param=foo;", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 4.0, UNKNOWN, UNKNOWN, NULL },
      /* Check for multiple trailing ';' */
      { "geo:0,3;param=foo;;;;", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 3.0, UNKNOWN, UNKNOWN, NULL },
      /* Check for multiple ';' used as separators */
      { "geo:5,1;;;;;param=foo;", TRP_PLACE_ERROR_INVALID_URI,
        5.0, 1.0, UNKNOWN, UNKNOWN, NULL },
      /* Check for multiple ';' everywhere */
      { "geo:6,1;;;param=foo;;;;", TRP_PLACE_ERROR_INVALID_URI,
        6.0, 1.0, UNKNOWN, UNKNOWN, NULL },
      { "place:føö", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "føö" },
      /* Unescaped commas are not a paramchar in RFC 5870 */
      { "place:X;location=geo:0,1", TRP_PLACE_ERROR_INVALID_URI,
        0.0, 1.0, UNKNOWN, UNKNOWN, "X" },
      /* Unescaped equals isn't a paramchar, so this fails with "expected a value
   * for uncertainty parameter" because the equals is treated as going back
   * to the outer place: URI */
      { "place:Hello%20world;location=geo:-0.5%2C-1.25%2C-2.125%3Bu=100",
        TRP_PLACE_ERROR_INVALID_URI,
        -0.5, -1.25, -2.125, 100.0, "Hello world" },
      /* Check for trailing ';' */
      { "place:X;location=geo:9,1;", TRP_PLACE_ERROR_INVALID_URI,
        9.0, 1.0, UNKNOWN, UNKNOWN, "X" },
      /* Check for multiple ';' */
      { "place:X;;;location=geo:4,2;;;", TRP_PLACE_ERROR_INVALID_URI,
        4.0, 2.0, UNKNOWN, UNKNOWN, "X" },
      { "place:Whatever;postal-code", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;postal-code=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;country", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;country=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;street", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;street=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;building", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;building=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;formatted-address", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;formatted-address=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;area", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;area=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;region", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;region=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;locality", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:Whatever;locality=", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "Whatever" },
      { "place:a%ffb", TRP_PLACE_ERROR_INVALID_URI,
        UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, "a�b" },
    };

static void
test_sample_uris (const SampleUri *samples, gsize len, TrpUriFlags flags)
{
  gsize i;

  for (i = 0; i < len; i++)
    {
      const SampleUri *sample = samples + i;
      g_autoptr (GError) error = NULL;
      g_autoptr (TrpPlace) place = trp_place_new_for_uri (sample->uri,
                                                          flags,
                                                          &error);
      g_autofree gchar *location_string = NULL;
      g_autofree gchar *building = NULL;
      g_autofree gchar *street = NULL;
      g_autofree gchar *area = NULL;
      g_autofree gchar *locality = NULL;
      g_autofree gchar *region = NULL;
      g_autofree gchar *country = NULL;
      g_autofree gchar *postal_code = NULL;
      g_autofree gchar *formatted_address = NULL;
      gdouble latitude, longitude, altitude, uncertainty;

      g_test_message ("%s (%s)", sample->uri,
                      (flags == TRP_URI_FLAGS_STRICT) ? "strict" : "none");
      g_assert_no_error (error);

      g_object_get (place,
                    "location-string", &location_string,
                    "latitude", &latitude,
                    "longitude", &longitude,
                    "altitude", &altitude,
                    "uncertainty", &uncertainty,
                    "building", &building,
                    "street", &street,
                    "area", &area,
                    "locality", &locality,
                    "region", &region,
                    "country", &country,
                    "postal-code", &postal_code,
                    "formatted-address", &formatted_address,
                    NULL);

      g_assert_cmpfloat (latitude, ==, sample->latitude);
      g_assert_cmpfloat (longitude, ==, sample->longitude);
      g_assert_cmpfloat (altitude, ==, sample->altitude);
      g_assert_cmpfloat (uncertainty, ==, sample->uncertainty);
      g_assert_cmpstr (location_string, ==, sample->location_string);
      g_assert_cmpstr (building, ==, sample->building);
      g_assert_cmpstr (street, ==, sample->street);
      g_assert_cmpstr (area, ==, sample->area);
      g_assert_cmpstr (locality, ==, sample->locality);
      g_assert_cmpstr (region, ==, sample->region);
      g_assert_cmpstr (country, ==, sample->country_code);
      g_assert_cmpstr (postal_code, ==, sample->postal_code);
      g_assert_cmpstr (formatted_address, ==, sample->formatted_address);
    }
}

static void
test_from_uri (void)
{
  test_sample_uris (good_uris, G_N_ELEMENTS (good_uris), TRP_URI_FLAGS_STRICT);
  test_sample_uris (good_uris, G_N_ELEMENTS (good_uris), TRP_URI_FLAGS_NONE);
  test_sample_uris (ugly_uris, G_N_ELEMENTS (ugly_uris), TRP_URI_FLAGS_NONE);
}

static void
test_from_invalid_uri (void)
{
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (bad_uris); i++)
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (TrpPlace) place = trp_place_new_for_uri (bad_uris[i].uri,
                                                          TRP_URI_FLAGS_NONE,
                                                          &error);

      g_test_message ("%s (none)", bad_uris[i].uri);
      g_assert_error (error, TRP_PLACE_ERROR, bad_uris[i].code);
      g_assert_null (place);
      g_test_message ("-> %s", error->message);
    }

  for (i = 0; i < G_N_ELEMENTS (bad_uris); i++)
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (TrpPlace) place = trp_place_new_for_uri (bad_uris[i].uri,
                                                          TRP_URI_FLAGS_STRICT,
                                                          &error);

      g_test_message ("%s (strict)", bad_uris[i].uri);
      g_assert_error (error, TRP_PLACE_ERROR, bad_uris[i].code);
      g_assert_null (place);
      g_test_message ("-> %s", error->message);
    }

  for (i = 0; i < G_N_ELEMENTS (differently_bad_uris); i++)
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (TrpPlace) place =
          trp_place_new_for_uri (differently_bad_uris[i].uri,
                                 TRP_URI_FLAGS_NONE, &error);

      g_test_message ("%s (none)", differently_bad_uris[i].uri);
      g_assert_error (error, TRP_PLACE_ERROR,
                      differently_bad_uris[i].code_when_tolerant);
      g_assert_null (place);
      g_test_message ("-> %s", error->message);
    }

  for (i = 0; i < G_N_ELEMENTS (differently_bad_uris); i++)
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (TrpPlace) place =
          trp_place_new_for_uri (differently_bad_uris[i].uri,
                                 TRP_URI_FLAGS_STRICT, &error);

      g_test_message ("%s (strict)", differently_bad_uris[i].uri);
      g_assert_error (error, TRP_PLACE_ERROR,
                      differently_bad_uris[i].code_when_strict);
      g_assert_null (place);
      g_test_message ("-> %s", error->message);
    }

  /* These do not parse successfully with the STRICT flag */
  for (i = 0; i < G_N_ELEMENTS (ugly_uris); i++)
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (TrpPlace) place = trp_place_new_for_uri (ugly_uris[i].uri,
                                                          TRP_URI_FLAGS_STRICT,
                                                          &error);

      g_test_message ("%s (strict)", ugly_uris[i].uri);
      g_assert_error (error, TRP_PLACE_ERROR, ugly_uris[i].code);
      g_assert_null (place);
      g_test_message ("-> %s", error->message);
    }
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/place/macros", test_macros);
  g_test_add_func ("/place/builder", test_builder);
  g_test_add_func ("/place/to-uri", test_to_uri);
  g_test_add_func ("/place/from-uri", test_from_uri);
  g_test_add_func ("/place/from-invalid-uri", test_from_invalid_uri);

  return g_test_run ();
}
