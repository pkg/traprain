/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include "dbus/org.apertis.Navigation1.h"
#include "test-helper.h"
#include "traprain-common/languages-map-internal.h"
#include "traprain-service/navigation.h"
#include "traprain-service/route-internal.h"

#define NAVIGATION_ROUTES_PATH "/org/apertis/Navigation1/Routes"
#define NAVIGATION_BUS_NAME "org.apertis.Navigation1"

typedef struct
{
  GTestDBus *dbus;
  GDBusConnection *conn; /* owned */
  GMainLoop *loop;
  GError *error;
  gboolean init_result;

  TrpServiceNavigation *nav;

  TrpNavigation1Routes *routes_proxy;
  TrpNavigation1Route *route_proxy;
} Test;

static void
setup (Test *test, gconstpointer unused)
{
  const gchar *addr;

  test->dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
  g_test_dbus_up (test->dbus);

  addr = g_test_dbus_get_bus_address (test->dbus);
  g_assert (addr != NULL);

  test->conn = g_dbus_connection_new_for_address_sync (addr,
                                                       G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                           G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                       NULL, NULL, &test->error);

  test->nav = trp_service_navigation_new (test->conn);
  g_assert (TRP_SERVICE_IS_NAVIGATION (test->nav));

  test->loop = g_main_loop_new (NULL, FALSE);
  test->error = NULL;
}

static void
teardown (Test *test, gconstpointer unused)
{
  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  g_clear_object (&test->nav);

  g_clear_object (&test->routes_proxy);
  g_clear_object (&test->route_proxy);

  g_test_dbus_down (test->dbus);
  g_clear_object (&test->dbus);
  g_clear_object (&test->conn);
}

static void
init_cb (GObject *source, GAsyncResult *res, gpointer user_data)
{
  Test *test = user_data;

  test->init_result = g_async_initable_init_finish (G_ASYNC_INITABLE (source), res, &test->error);
  g_main_loop_quit (test->loop);
}

static void
routes_proxy_cb (GObject *source, GAsyncResult *res, gpointer user_data)
{
  Test *test = user_data;

  test->routes_proxy = trp_navigation1_routes_proxy_new_for_bus_finish (res, &test->error);

  g_main_loop_quit (test->loop);
}

static void
create_routes_proxy (Test *test)
{
  g_clear_object (&test->routes_proxy);

  trp_navigation1_routes_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START,
                                    NAVIGATION_BUS_NAME, NAVIGATION_ROUTES_PATH,
                                    NULL, routes_proxy_cb, test);

  g_main_loop_run (test->loop);
}

static void
init_service (Test *test)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->nav), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert (test->init_result);
  g_assert_no_error (test->error);
}

static void
test_init (Test *test, gconstpointer unused)
{
  g_autofree gchar *owner = NULL;

  /* Service is not yet initialized, so not yet exposed on the bus */
  create_routes_proxy (test);
  g_assert_no_error (test->error);
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (test->routes_proxy));
  g_assert (owner == NULL);

  init_service (test);

  /* Check that the proxy is owned now that the service is initialized */
  create_routes_proxy (test);
  g_assert_no_error (test->error);
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (test->routes_proxy));
  g_assert (owner != NULL);
}

static void
invalidated_cb (TrpServiceNavigation *service,
                const GError *error,
                Test *test)
{
  g_assert (TRP_SERVICE_IS_NAVIGATION (service));
  g_assert (error != NULL);

  g_clear_error (&test->error);
  test->error = g_error_copy (error);
  g_main_loop_quit (test->loop);
}

static void
test_invalidated (Test *test, gconstpointer unused)
{
  GDBusConnection *conn;
  g_autoptr (GError) error = NULL;

  init_service (test);

  g_assert_no_error (test->error);
  g_signal_connect (test->nav, "invalidated", G_CALLBACK (invalidated_cb),
                    test);

  conn = trp_service_navigation_get_dbus_connection (test->nav);
  g_assert (G_IS_DBUS_CONNECTION (conn));
  g_dbus_connection_close (conn, NULL, NULL, NULL);
  g_main_loop_run (test->loop);

  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER);

  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_first_service_route (test->nav), -1, &error), ==, -1);
  g_assert_error (error, G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER);
}

static void
route_proxy_cb (GObject *source,
                GAsyncResult *res,
                gpointer user_data)
{
  Test *test = user_data;

  test->route_proxy = trp_navigation1_route_proxy_new_finish (res, &test->error);

  g_main_loop_quit (test->loop);
}

static void
create_route_proxy (Test *test,
                    const gchar *path)
{
  GDBusConnection *conn;

  g_assert (test->routes_proxy != NULL);
  conn = g_dbus_proxy_get_connection (G_DBUS_PROXY (test->routes_proxy));

  g_clear_object (&test->route_proxy);

  trp_navigation1_route_proxy_new (conn, G_DBUS_PROXY_FLAGS_NONE, NAVIGATION_BUS_NAME, path, NULL, route_proxy_cb, test);

  g_main_loop_run (test->loop);
}

static void
check_route (Test *test,
             const gchar *path,
             guint distance,
             guint time,
             GVariant *title,
             GVariant *geometry,
             GVariant *desc)
{
  GVariant *v;
  g_autoptr (GVariant) _title = NULL, _geometry = NULL, _desc = NULL;

  _title = g_variant_ref_sink (title);
  _geometry = g_variant_ref_sink (geometry);
  _desc = g_variant_ref_sink (desc);

  create_route_proxy (test, path);
  g_assert_no_error (test->error);

  g_assert_cmpuint (trp_navigation1_route_get_total_distance (test->route_proxy), ==, distance);
  g_assert_cmpuint (trp_navigation1_route_get_total_time (test->route_proxy), ==, time);

  v = trp_navigation1_route_get_title (test->route_proxy);
  assert_equal_variants (v, _title);

  v = trp_navigation1_route_get_geometry (test->route_proxy);
  assert_equal_variants (v, _geometry);

  v = trp_navigation1_route_get_geometry_descriptions (test->route_proxy);
  g_assert (v != NULL);

  assert_equal_variants (v, _desc);
}

static void
check_first_route (Test *test,
                   const gchar *path)
{
  check_route (test, path, FST_ROUTE_DISTANCE, FST_ROUTE_TIME,
               g_variant_new_parsed ("{ 'fr_FR':%s,'en_US':%s }", FST_ROUTE_TITLE_FR, FST_ROUTE_TITLE_EN),
               g_variant_new_parsed ("[ (%d, %d), (%d, %d) ]", (gdouble) FST_ROUTE_FST_SEG_LAT, (gdouble) FST_ROUTE_FST_SEG_LON,
                                     (gdouble) FST_ROUTE_SND_SEG_LAT, (gdouble) FST_ROUTE_SND_SEG_LON),
               g_variant_new_parsed ("{ %u: {'en_US': %s, 'fr_FR': %s}, %u: {'en_US': %s} }", 0, FST_ROUTE_FST_SEG_DESC_EN, FST_ROUTE_FST_SEG_DESC_FR, 1, FST_ROUTE_SND_SEG_DESC_EN));
}

static void
check_second_route (Test *test,
                    const gchar *path)
{
  check_route (test, path, SND_ROUTE_DISTANCE, SND_ROUTE_TIME,
               g_variant_new_parsed ("{ 'fr_FR':%s,'en_US':%s }", SND_ROUTE_TITLE_FR, SND_ROUTE_TITLE_EN),
               g_variant_new_parsed ("[ (%d, %d), (%d, %d) ]", (gdouble) SND_ROUTE_FST_SEG_LAT, (gdouble) SND_ROUTE_FST_SEG_LON,
                                     (gdouble) SND_ROUTE_SND_SEG_LAT, (gdouble) SND_ROUTE_SND_SEG_LON),
               g_variant_new_parsed ("{ %u: {'fr_FR': %s} }", 0, SND_ROUTE_FST_SEG_DESC_FR));
}

static void
check_third_route (Test *test,
                   const gchar *path)
{
  check_route (test, path, THD_ROUTE_DISTANCE, THD_ROUTE_TIME,
               g_variant_new_parsed ("{ 'en_US':%s }", THD_ROUTE_TITLE_EN),
               g_variant_new_parsed ("[ (%d, %d), (%d, %d) ]", (gdouble) THD_ROUTE_FST_SEG_LAT, (gdouble) THD_ROUTE_FST_SEG_LON,
                                     (gdouble) THD_ROUTE_SND_SEG_LAT, (gdouble) THD_ROUTE_SND_SEG_LON),
               g_variant_new_parsed ("{ %u: {'fr_FR':%s } }", 0, THD_ROUTE_FST_SEG_DESC_FR));
}

static void
notify_cb (GObject *object,
           GParamSpec *spec,
           Test *test)
{
  g_main_loop_quit (test->loop);
}

/* - Add some routes
 * - Init the services so routes are now published
 * - Add a third route
 * - Change current route
 */
static void
test_publish_pre_register (Test *test,
                           gconstpointer unused)
{
  GStrv routes;

  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 0);
  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_first_service_route (test->nav), -1, NULL), ==, 0);
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 1);
  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_second_service_route (test->nav), -1, NULL), ==, 1);
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 2);

  g_assert (trp_service_navigation_set_current_route (test->nav, 1, NULL));

  /* Service is registered so routes are published now */
  init_service (test);
  create_routes_proxy (test);

  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 2);

  check_first_route (test, routes[0]);
  check_second_route (test, routes[1]);
  g_strfreev (routes);

  g_assert_cmpint (trp_navigation1_routes_get_current_route (test->routes_proxy), ==, 1);

  /* Add Third route to the 2nd position and check if things are updated */
  g_signal_connect (test->routes_proxy, "notify::routes", G_CALLBACK (notify_cb), test);
  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_third_service_route (test->nav), 1, NULL), ==, 1);
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 3);
  g_main_loop_run (test->loop);

  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 3);

  check_first_route (test, routes[0]);
  check_third_route (test, routes[1]);
  check_second_route (test, routes[2]);
  g_strfreev (routes);

  /* Pick the first route as current */
  g_signal_connect (test->routes_proxy, "notify::current-route", G_CALLBACK (notify_cb), test);
  g_assert (trp_service_navigation_set_current_route (test->nav, 0, NULL));
  g_main_loop_run (test->loop);

  g_assert_cmpint (trp_navigation1_routes_get_current_route (test->routes_proxy), ==, 0);
}

/* - Init the service
 * - Add some routes and check things are updated
 */
static void
test_publish_post_register (Test *test,
                            gconstpointer unused)
{
  GStrv routes;

  init_service (test);
  create_routes_proxy (test);

  /* No route published atm */
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 0);
  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 0);
  g_strfreev (routes);

  g_assert_cmpint (trp_navigation1_routes_get_current_route (test->routes_proxy), ==, -1);

  g_signal_connect (test->routes_proxy, "notify::routes", G_CALLBACK (notify_cb), test);
  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_first_service_route (test->nav), -1, NULL), ==, 0);
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 1);
  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_second_service_route (test->nav), -1, NULL), ==, 1);
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 2);
  g_main_loop_run (test->loop);

  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 2);

  check_first_route (test, routes[0]);
  check_second_route (test, routes[1]);

  g_strfreev (routes);
}

/* - Add a couple of routes
 * - Init the service
 * - Remove the first route
 */
static void
test_remove_route (Test *test,
                   gconstpointer unused)
{
  GStrv routes;
  g_autofree gchar *first_route_path = NULL;

  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_first_service_route (test->nav), -1, NULL), ==, 0);
  g_assert_cmpuint (trp_service_navigation_add_route (test->nav, create_second_service_route (test->nav), -1, NULL), ==, 1);
  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 2);

  /* Service is registered so routes are published now */
  init_service (test);
  create_routes_proxy (test);

  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 2);

  first_route_path = g_strdup (routes[0]);
  g_strfreev (routes);

  /* Remove first route */
  g_signal_connect (test->routes_proxy, "notify", G_CALLBACK (notify_cb), test);
  g_assert (trp_service_navigation_remove_route (test->nav, 0, NULL));
  g_main_loop_run (test->loop);

  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 1);
  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 1);

  check_second_route (test, routes[0]);
  g_strfreev (routes);

  /* Check that the first route object has been removed from the bus */
  create_route_proxy (test, first_route_path);
  g_assert_cmpuint (trp_navigation1_route_get_total_distance (test->route_proxy), ==, 0);
}

/* - Add 2 routes in one shot
 * - Init the service
 */
static void
test_set_routes (Test *test,
                 gconstpointer unused)
{
  GStrv routes;
  GPtrArray *arr;

  arr = g_ptr_array_new_with_free_func (g_object_unref);
  g_ptr_array_add (arr, create_first_service_route (test->nav));
  g_ptr_array_add (arr, create_second_service_route (test->nav));

  g_assert (trp_service_navigation_set_routes (test->nav, arr, NULL));

  /* Service is registered so routes are published now */
  init_service (test);
  create_routes_proxy (test);

  g_assert_cmpuint (trp_service_navigation_get_n_routes (test->nav), ==, 2);

  routes = trp_navigation1_routes_dup_routes (test->routes_proxy);
  g_assert_cmpuint (g_strv_length (routes), ==, 2);

  check_first_route (test, routes[0]);
  check_second_route (test, routes[1]);
  g_strfreev (routes);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/service/navigation/init", Test, NULL,
              setup, test_init, teardown);
  g_test_add ("/service/navigation/invalidated", Test, NULL,
              setup, test_invalidated, teardown);
  g_test_add ("/service/navigation/publish-pre-register", Test, NULL,
              setup, test_publish_pre_register, teardown);
  g_test_add ("/service/navigation/publish-post-register", Test, NULL,
              setup, test_publish_post_register, teardown);
  g_test_add ("/service/navigation/remove-route", Test, NULL,
              setup, test_remove_route, teardown);
  g_test_add ("/service/navigation/set-routes", Test, NULL,
              setup, test_set_routes, teardown);

  return g_test_run ();
}
