/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_CLIENT_MOCK_H__
#define __TRAPRAIN_CLIENT_MOCK_H__

#include <gio/gio.h>
#include <glib-object.h>

#include "traprain-common/route.h"

G_BEGIN_DECLS

#define TRP_CLIENT_TYPE_MOCK (trp_client_mock_get_type ())
G_DECLARE_FINAL_TYPE (TrpClientMock, trp_client_mock, TRP_CLIENT, MOCK, GObject)

void trp_client_mock_new_async (GDBusConnection *connection,
                                GCancellable *cancellable,
                                GAsyncReadyCallback callback,
                                gpointer user_data);

TrpClientMock *trp_client_mock_new_finish (GAsyncResult *result,
                                           GError **error);

TrpRoute *trp_client_mock_create_route (TrpClientMock *self,
                                        guint total_distance,
                                        guint total_time);

void trp_client_mock_add_route_async (TrpClientMock *self,
                                      TrpRoute *route,
                                      GAsyncReadyCallback callback,
                                      gpointer user_data);

gint trp_client_mock_add_route_finish (TrpClientMock *self,
                                       GAsyncResult *result,
                                       GError **error);

void trp_client_mock_remove_route_async (TrpClientMock *self,
                                         guint index,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data);

gboolean trp_client_mock_remove_route_finish (TrpClientMock *self,
                                              GAsyncResult *result,
                                              GError **error);

void trp_client_mock_set_current_route_async (TrpClientMock *self,
                                              guint index,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);

gboolean trp_client_mock_set_current_route_finish (TrpClientMock *self,
                                                   GAsyncResult *result,
                                                   GError **error);

void trp_client_mock_clear_routes_async (TrpClientMock *self,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data);

gboolean trp_client_mock_clear_routes_finish (TrpClientMock *self,
                                              GAsyncResult *result,
                                              GError **error);

void trp_client_mock_set_start_time_async (TrpClientMock *self,
                                           GDateTime *start_time,
                                           GAsyncReadyCallback callback,
                                           gpointer user_data);

gboolean trp_client_mock_set_start_time_finish (TrpClientMock *self,
                                                GAsyncResult *result,
                                                GError **error);

void trp_client_mock_set_estimated_end_time_async (TrpClientMock *self,
                                                   GDateTime *estimated_end_time,
                                                   GAsyncReadyCallback callback,
                                                   gpointer user_data);

gboolean trp_client_mock_set_estimated_end_time_finish (TrpClientMock *self,
                                                        GAsyncResult *result,
                                                        GError **error);

G_END_DECLS

#endif /* __TRAPRAIN_CLIENT_MOCK_H__ */
