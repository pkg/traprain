/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <gio/gio.h>
#include <libnotify/notify.h>

#include "guidance-ui.h"

#define APP_NAME "org.apertis.Traprain1.GuidanceUI"

static void
on_startup (GApplication *application)
{
  TrpGuidanceUI *guidance = TRP_GUIDANCE_UI (application);

  g_debug ("startup");

  trp_guidance_ui_start (guidance);
}

static void
on_shutdown (GApplication *application)
{
  TrpGuidanceUI *guidance = TRP_GUIDANCE_UI (application);

  g_debug ("shutdown");

  trp_guidance_ui_stop (guidance);
}

int
main (int argc,
      char *argv[])
{
  g_autoptr (TrpGuidanceUI) service = NULL;
  GApplication *application = NULL;

  /* TODO: once T3303 is fixed, it would be good to switch to GNotification */
  g_assert (notify_init (APP_NAME));

  service = trp_guidance_ui_new (APP_NAME, G_APPLICATION_IS_SERVICE);

  application = G_APPLICATION (service);
  g_application_hold (application);

  g_signal_connect (application, "startup", G_CALLBACK (on_startup), NULL);
  g_signal_connect (application, "shutdown", G_CALLBACK (on_shutdown), NULL);

  return g_application_run (application, argc, argv);
}
