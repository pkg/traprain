/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "navigation-internal.h"
#include "navigation.h"
#include "route-internal.h"

#include "dbus/org.apertis.Navigation1.h"

#define NAVIGATION_ROUTES_PATH "/org/apertis/Navigation1/Routes"
#define NAVIGATION_BUS_NAME "org.apertis.Navigation1"

#define NAVIGATION_ROUTE_PATH "/org/apertis/Navigation1/Routes/%u"

/**
 * SECTION: traprain-service/navigation.h
 * @title: TrpServiceNavigation
 * @short_description: Publish routes information to external applications
 *
 * #TrpServiceNavigation is used to publish navigation routes information
 * to externa appplications.
 *
 * A navigation service should create one #TrpServiceNavigation during its
 * whole lifetime and use it to publish information about the current and
 * potential navigation routes to external applications.
 *
 * Note that nothing is published until g_async_initable_init_async()
 * has been called.
 *
 * Since: 0.1.0
 */

/**
 * TrpServiceNavigation:
 *
 * Object used to publish routes information to application developers.
 *
 * Since: 0.1.0
 */

struct _TrpServiceNavigation
{
  GObject parent;

  TrpNavigation1Routes *service; /* owned */
  GDBusConnection *conn;         /* owned */
  GCancellable *cancellable;     /* owned */
  GList *init_tasks;             /* owned */
  gboolean registered;
  guint own_name_id;                    /* 0 if the server is not registered yet */
  GDBusObjectManagerServer *object_mgr; /* owned */
  GError *invalidated_error;            /* owned, NULL until the service is invalidated */

  GPtrArray /*<owned _TrpServiceRoute>*/ *routes;           /* owned */
  GHashTable /*<owned _TrpServiceRoute>*/ *exported_routes; /* owned, used as a set */
  guint32 route_path_count;                                 /* id used to generated unique Route paths */
  gint current_route;
  guint update_id;
};

typedef enum {
  PROP_CONNECTION = 1,
  /*< private >*/
  PROP_LAST = PROP_CONNECTION
} TrpServiceNavigationProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (TrpServiceNavigation, trp_service_navigation, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
trp_service_navigation_get_property (GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
  TrpServiceNavigation *self = TRP_SERVICE_NAVIGATION (object);

  switch ((TrpServiceNavigationProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_service_navigation_set_property (GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
  TrpServiceNavigation *self = TRP_SERVICE_NAVIGATION (object);

  switch ((TrpServiceNavigationProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_service_navigation_dispose (GObject *object)
{
  TrpServiceNavigation *self = (TrpServiceNavigation *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  if (self->registered)
    {
      g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self->service));
      self->registered = FALSE;
    }

  g_clear_object (&self->service);
  g_clear_object (&self->conn);

  if (self->init_tasks != NULL)
    {
      g_list_free_full (self->init_tasks, g_object_unref);
      self->init_tasks = NULL;
    }

  g_clear_pointer (&self->routes, g_ptr_array_unref);
  g_clear_pointer (&self->exported_routes, g_hash_table_unref);
  g_clear_object (&self->object_mgr);
  g_clear_error (&self->invalidated_error);

  G_OBJECT_CLASS (trp_service_navigation_parent_class)
      ->dispose (object);
}

static void
trp_service_navigation_class_init (TrpServiceNavigationClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_service_navigation_get_property;
  object_class->set_property = trp_service_navigation_set_property;
  object_class->dispose = trp_service_navigation_dispose;

  /**
   * TrpServiceNavigation:connection:
   *
   * The #GDBusConnection used by the service to publish routes.
   *
   * Since: 0.1.0
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * TrpServiceNavigation::invalidated:
   * @self: a #TrpServiceNavigation
   * @error: error which caused @self to be invalidated
   *
   * Emitted when the backing object underlying this #TrpServiceNavigation
   * disappears, or it is otherwise disconnected.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus service name is lost.
   *
   * After this signal is emitted, all method calls to #TrpServiceNavigation methods will
   * fail with the same error as the one reported in the signal (usually
   * #G_DBUS_ERROR_NAME_HAS_NO_OWNER).
   *
   * Since: 0.1.0
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", TRP_SERVICE_TYPE_NAVIGATION,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
trp_service_navigation_init (TrpServiceNavigation *self)
{
  self->service = trp_navigation1_routes_skeleton_new ();
  self->cancellable = g_cancellable_new ();

  self->routes = g_ptr_array_new_with_free_func (g_object_unref);
  self->exported_routes = g_hash_table_new_full (NULL, NULL, g_object_unref, NULL);
  self->current_route = -1;
}

/**
 * trp_service_navigation_new:
 * @connection: the #GDBusConnection to use to publish routes
 *
 * Create a new #TrpServiceNavigation. This service won't expose anything
 * on D-Bus until g_async_initable_init_async() has been called.
 *
 * Returns: (transfer full): a new #TrpServiceNavigation
 * Since: 0.1.0
 */
TrpServiceNavigation *
trp_service_navigation_new (GDBusConnection *connection)
{
  return g_object_new (TRP_SERVICE_TYPE_NAVIGATION, "connection", connection, NULL);
}

static void
fail_init_tasks (TrpServiceNavigation *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

/**
 * trp_service_navigation_create_route:
 * @total_distance: the total distance covered by this route, in metres
 * @total_time: an estimation of the time needed to travel this route, in seconds
 *
 * Create a new TrpRoute representing a potential navigation route.
 *
 * One should generally create a route, set its title using trp_route_add_title()
 * and add the geometry of the route using trp_route_add_segment().
 * Once the route has been fully defined it should be passed to the #TrpServiceNavigation
 * for publishing using either trp_service_navigation_add_route() or
 * trp_service_navigation_set_routes().
 *
 * Returns: (transfer full): a new #TrpRoute
 * Since: 0.1.0
 */
TrpRoute *
trp_service_navigation_create_route (TrpServiceNavigation *self,
                                     guint total_distance,
                                     guint total_time)
{
  return _trp_service_route_new (total_distance, total_time);
}

static void
complete_init_tasks (TrpServiceNavigation *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static gboolean
service_route_export (TrpServiceNavigation *self,
                      _TrpServiceRoute *service_route)
{
  TrpRoute *route = (TrpRoute *) service_route;
  g_autofree gchar *path = NULL;
  GVariant *title, *geometry, *geometry_desc;

  g_return_val_if_fail (service_route->service == NULL, FALSE);
  g_return_val_if_fail (self->service != NULL, FALSE);

  if (self->route_path_count == G_MAXUINT32)
    {
      g_warning ("route path count overflow");
      return FALSE;
    }

  path = g_strdup_printf (NAVIGATION_ROUTE_PATH, self->route_path_count++);

  service_route->object = trp_object_skeleton_new (path);
  service_route->service = trp_navigation1_route_skeleton_new ();
  route->writable = FALSE;
  trp_object_skeleton_set_navigation1_route (service_route->object, service_route->service);

  _trp_route_to_variants (route, &title, &geometry, &geometry_desc);

  trp_navigation1_route_set_total_distance (service_route->service, route->total_distance);
  trp_navigation1_route_set_total_time (service_route->service, route->total_time);
  trp_navigation1_route_set_title (service_route->service, title);
  trp_navigation1_route_set_geometry (service_route->service, geometry);
  trp_navigation1_route_set_geometry_descriptions (service_route->service, geometry_desc);

  g_dbus_object_manager_server_export (self->object_mgr, G_DBUS_OBJECT_SKELETON (service_route->object));

  return TRUE;
}

static gboolean
purge_old_routes (gpointer k,
                  gpointer v,
                  gpointer user_data)
{
  TrpServiceNavigation *self = user_data;
  _TrpServiceRoute *route = v;

  g_assert (route->object != NULL);

  if (route->update_id == self->update_id)
    return FALSE;

  g_dbus_object_manager_server_unexport (self->object_mgr,
                                         g_dbus_object_get_object_path (G_DBUS_OBJECT (route->object)));

  return TRUE;
}

static void
update_routes (TrpServiceNavigation *self)
{
  g_autoptr (GPtrArray) routes = NULL;
  guint i;

  routes = g_ptr_array_new ();

  /* Don't care about overflows as this ID is just used to compare one
   * epoch and a subsequent one. */
  self->update_id++;

  for (i = 0; i < self->routes->len; i++)
    {
      _TrpServiceRoute *r = g_ptr_array_index (self->routes, i);

      if (r->service == NULL)
        {
          if (!service_route_export (self, r))
            continue;

          g_hash_table_add (self->exported_routes, g_object_ref (r));
        }

      r->update_id = self->update_id;
      g_ptr_array_add (routes, (gpointer) g_dbus_object_get_object_path (G_DBUS_OBJECT (r->object)));
    }
  g_ptr_array_add (routes, NULL);

  /* Stop exposing routes which have been removed */
  g_hash_table_foreach_remove (self->exported_routes, purge_old_routes, self);

  trp_navigation1_routes_set_routes (self->service, (const gchar *const *) routes->pdata);
}

static void
update_current_route (TrpServiceNavigation *self)
{
  trp_navigation1_routes_set_current_route (self->service, self->current_route);
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  TrpServiceNavigation *self = user_data;
  GError *error = NULL;

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->service),
                                         conn, NAVIGATION_ROUTES_PATH, &error))
    {
      fail_init_tasks (self, error);
    }
  else
    {
      self->object_mgr = g_dbus_object_manager_server_new (NAVIGATION_ROUTES_PATH);

      update_routes (self);
      update_current_route (self);

      g_dbus_object_manager_server_set_connection (self->object_mgr, conn);
      self->registered = TRUE;
      complete_init_tasks (self);
    }
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  TrpServiceNavigation *self = user_data;

  g_clear_error (&self->invalidated_error);
  self->invalidated_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER, "Lost bus name '%s'", name);

  fail_init_tasks (self, self->invalidated_error);
  g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
}

static void
trp_service_navigation_init_async (GAsyncInitable *initable,
                                   int io_priority,
                                   GCancellable *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer user_data)
{
  TrpServiceNavigation *self = TRP_SERVICE_NAVIGATION (initable);
  GTask *task;
  gboolean start_init;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->registered)
    {
      g_task_return_boolean (task, TRUE);
      g_object_unref (task);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, task);

  if (start_init)
    self->own_name_id = g_bus_own_name_on_connection (self->conn,
                                                      NAVIGATION_BUS_NAME,
                                                      G_BUS_NAME_OWNER_FLAGS_NONE,
                                                      bus_name_acquired_cb,
                                                      bus_name_lost_cb, self,
                                                      NULL);
}

static gboolean
trp_service_navigation_init_finish (GAsyncInitable *initable,
                                    GAsyncResult *result,
                                    GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = trp_service_navigation_init_async;
  iface->init_finish = trp_service_navigation_init_finish;
}

/**
 * trp_service_navigation_get_dbus_connection:
 * @self: a #TrpServiceNavigation
 *
 * Return the #GDBusConnection used by @self to export routes
 * information on the bus, or %NULL if @self has not been
 * initialized yet.
 *
 * Returns: (transfer none) (nullable): a #GDBusConnection
 * Since: 0.1.0
 */
GDBusConnection *
trp_service_navigation_get_dbus_connection (TrpServiceNavigation *self)
{
  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION (self), NULL);

  return self->conn;
}

static gboolean
check_invalidated (TrpServiceNavigation *self,
                   GError **error)
{
  if (self->invalidated_error == NULL)
    return TRUE;

  if (error != NULL)
    *error = g_error_copy (self->invalidated_error);

  return FALSE;
}

/**
 * trp_service_navigation_add_route:
 * @self: a #TrpServiceNavigation
 * @route: (transfer full): a #TrpRoute
 * @index: the index to place the new route at, or -1 to append
 * @error: return location for error or %NULL.
 *
 * Inserts @route into the list of potential routes at the given index.
 * The most highly recommended route must be placed first, so at the index 0.
 * @route should contain at least two segments: the start and destination of
 * the route.
 *
 * This function takes ownership of @route so it should no longer be used or modified
 * by the caller.
 *
 * Returns: the index of the newly added route, or -1 if an error occurred
 * Since: 0.1.0
 */
gint
trp_service_navigation_add_route (TrpServiceNavigation *self,
                                  TrpRoute *route,
                                  gint index,
                                  GError **error)
{
  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION (self), -1);
  g_return_val_if_fail (route != NULL, -1);
  g_return_val_if_fail (index == -1 || (index >= 0 && (guint) index < self->routes->len), -1);
  g_return_val_if_fail (route->geometry->len >= 2, -1);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);

  if (!check_invalidated (self, error))
    {
      g_object_unref (route);
      return -1;
    }

  g_ptr_array_insert (self->routes, index, route);

  if (self->registered)
    {
      update_routes (self);
    }

  return index == -1 ? self->routes->len - 1 : (guint) index;
}

/**
 * trp_service_navigation_remove_route:
 * @self: a #TrpServiceNavigation
 * @index: the index of the route to remove
 * @error: return location for error or %NULL.
 *
 * Remove the route at index @index from the list of potential routes.
 *
 * Returns: %TRUE if the operation succeeded
 * Since: 0.1.0
 */
gboolean
trp_service_navigation_remove_route (TrpServiceNavigation *self,
                                     guint index,
                                     GError **error)
{
  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION (self), FALSE);
  g_return_val_if_fail (index < self->routes->len, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);

  if (!check_invalidated (self, error))
    return FALSE;

  g_ptr_array_remove_index (self->routes, index);

  if (self->registered)
    {
      update_routes (self);
    }

  return TRUE;
}

/**
 * trp_service_navigation_set_routes:
 * @self: a #TrpServiceNavigation
 * @routes: (element-type TrpRoute) (transfer container): an array of #TrpRoute
 * @error: return location for error or %NULL.
 *
 * Set the list of potential routes to @routes, discarding previously added routes.
 * The most highly recommended routes should be placed first in @routes.
 *
 * This function takes ownership of @routes so the array should no longer be
 * used or modified by the caller.
 * @routes is expected to have an appropriate `element_free_func` defined, using
 * `g_ptr_array_new_with_free_func (g_object_unref)`
 * for example.
 *
 * Returns: %TRUE if the operation succeeded
 * Since: 0.1.0
 */
gboolean
trp_service_navigation_set_routes (TrpServiceNavigation *self,
                                   GPtrArray *routes,
                                   GError **error)
{
  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION (self), FALSE);
  g_return_val_if_fail (routes != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);

  if (!check_invalidated (self, error))
    {
      g_ptr_array_unref (routes);
      return FALSE;
    }

  g_ptr_array_unref (self->routes);
  self->routes = routes;

  if (self->registered)
    {
      update_routes (self);
    }

  return TRUE;
}

/**
 * trp_service_navigation_set_current_route:
 * @self: a #TrpServiceNavigation
 * @index: the index of the route to set as current, or -1
 * @error: return location for error or %NULL.
 *
 * Set the route at @index as the current one.
 * Passing -1 as @index indicates that there is currently no active road.
 *
 * Returns: %TRUE if the operation succeeded
 * Since: 0.1.0
 */
gboolean
trp_service_navigation_set_current_route (TrpServiceNavigation *self,
                                          gint index,
                                          GError **error)
{
  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION (self), FALSE);
  g_return_val_if_fail (index == -1 || (index >= 0 && (guint) index < self->routes->len), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);

  if (!check_invalidated (self, error))
    return FALSE;

  self->current_route = index;

  if (self->registered)
    {
      update_current_route (self);
    }

  return TRUE;
}

/**
 * trp_service_navigation_get_n_routes:
 * @self: a #TrpServiceNavigation
 *
 * Return the number of routes currently published by the service.
 *
 * Returns: the number of routes published by @self
 * Since: 0.1.0
 */
guint
trp_service_navigation_get_n_routes (TrpServiceNavigation *self)
{
  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION (self), 0);

  return self->routes->len;
}
