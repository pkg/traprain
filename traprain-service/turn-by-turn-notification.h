/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_SERVICE_TURN_BY_TURN_NOTIFICATION_H__
#define __TRAPRAIN_SERVICE_TURN_BY_TURN_NOTIFICATION_H__

#include <gio/gio.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define TRP_SERVICE_TYPE_TURN_BY_TURN_NOTIFICATION (trp_service_turn_by_turn_notification_get_type ())
G_DECLARE_FINAL_TYPE (TrpServiceTurnByTurnNotification, trp_service_turn_by_turn_notification, TRP_SERVICE, TURN_BY_TURN_NOTIFICATION, GObject)

TrpServiceTurnByTurnNotification *trp_service_turn_by_turn_notification_new (GDBusConnection *conn,
                                                                             const gchar *summary,
                                                                             const gchar *body,
                                                                             const gchar *icon);

void trp_service_turn_by_turn_notification_set_summary (TrpServiceTurnByTurnNotification *self,
                                                        const gchar *summary);
void trp_service_turn_by_turn_notification_set_body (TrpServiceTurnByTurnNotification *self,
                                                     const gchar *body);
void trp_service_turn_by_turn_notification_set_icon (TrpServiceTurnByTurnNotification *self,
                                                     const gchar *icon);

void trp_service_turn_by_turn_notification_send_async (TrpServiceTurnByTurnNotification *self,
                                                       gint expire_timeout,
                                                       GAsyncReadyCallback callback,
                                                       gpointer user_data);

gboolean trp_service_turn_by_turn_notification_send_finish (TrpServiceTurnByTurnNotification *self,
                                                            GAsyncResult *result,
                                                            GError **error);

void trp_service_turn_by_turn_notification_close_async (TrpServiceTurnByTurnNotification *self,
                                                        GAsyncReadyCallback callback,
                                                        gpointer user_data);

gboolean trp_service_turn_by_turn_notification_close_finish (TrpServiceTurnByTurnNotification *self,
                                                             GAsyncResult *result,
                                                             GError **error);

G_END_DECLS

#endif /* __TRAPRAIN_SERVICE_TURN_BY_TURN_NOTIFICATION_H__ */
