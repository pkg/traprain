/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_SERVICE_NAVIGATION_INTERNAL_H__
#define __TRAPRAIN_SERVICE_NAVIGATION_INTERNAL_H__

#include "dbus/org.apertis.Navigation1.h"
#include "navigation.h"
#include "traprain-common/languages-map-internal.h"

G_BEGIN_DECLS

G_END_DECLS

#endif /* __TRAPRAIN_SERVICE_NAVIGATION_INTERNAL_H__ */
